#!/usr/bin/python

import pygame
#import RPi.GPIO as GPIO
import lib.Cluster as Cluster
import lib.CAN_Poller as CAN_Poller
from socket import error as socket_error
from subprocess import call


def main():
#------ Setup GPIO pins ------#
#    GPIO.setmode(GPIO.BCM)
#    GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
#------ Button setup stuff ------#
#    startTime = pygame.time.get_ticks()
#    endTime = startTime
#    buttonState = 0

#------ Setup the cluster screen ------#
    cluster = Cluster.Cluster()
    cluster.info = "Loading"
    cluster.generateScreen()
    
#------ Setup CANbus connection ------#
    can = CAN_Poller.CAN_Poller()
    can.start()

#------ Start main compute loop ------#
    loop = True
    try:
        while loop:
        
        #------ Check for a button press ------#
        #   input_state = GPIO.input(23)
        #    if input_state == False:
        #        endTime = pygame.time.get_ticks()
        #        if endTime - startTime > 1000:
        #            if buttonState < 4:
        #                buttonState = buttonState + 1
        #            else:
        #                buttonState = 0
        #            startTime = pygame.time.get_ticks()

        #------ Get CAN values if CAN is detected ------#
            if can.readError == False and can.syncError == False:
                cluster.info = "All good"
                cluster.rpm = can.rpm
                cluster.speed = can.speed
                cluster.gear = can.gear
                cluster.coolantTmp = can.coolantTmp
                cluster.fuel = can.fuel
                cluster.boost = can.boost
                cluster.mil = can.mil
                cluster.voltage = can.voltage
                
                cluster.tps = can.tps
                cluster.afr = can.afr
                cluster.missCount = can.missCount
                
                cluster.iat = can.iat
                cluster.antilag = can.antilag
                  
            elif can.readError == 1:
                cluster.info = "CAN data read error"
            elif can.readError == 2:
                cluster.info = "Arduino connection failed!"
            elif can.syncError == True:
                cluster.info = "CAN sync Error!"
                
            #------ Check for keyboard press ------#
            for event in pygame.event.get():
                if event.type == pygame.KEYUP or event.type == pygame.QUIT:
                    if event.key == pygame.K_q:
                        can.threadLoop = False
                        loop = False

            #------ Generate the cluster screen now that we have all our values ------#
            cluster.generateScreen()        
            cluster.clock.tick(30)

    except KeyboardInterrupt:
        can.threadLoop = False


if __name__ == '__main__':
    main()
    
