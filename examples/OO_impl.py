#!/usr/bin/python

import pygame

WHITE = (255, 255, 255)
GREEN = (76, 175, 80)
RED = (244, 67, 54)
YELLOW = (255, 152, 0)
ORANGE = (255, 87, 34)
BLUE = (22, 150, 243)

class Cluster:
    def __init__(self):
        pygame.init()
        
        self.screenSize = (800,480)
        self.color = (0,0,0)
        self.mph = 99
        self.rpm = 9999
        self.fuel = 0
        self.fps = 0
        self.time = "--:--"

        self.screen = pygame.display.set_mode(self.screenSize) #, pygame.FULLSCREEN)
        self.clock = pygame.time.Clock()
        pygame.mouse.set_visible(False)

        # Setup screen placement values
        self.screenCenterX = self.screen.get_rect().centerx
        self.screenCenterY = self.screen.get_rect().centery
        self.screenHeight = self.screen.get_rect().height
        self.screenWidth = self.screen.get_rect().width

        # Setup all fonts
        self.clockFontLarge = pygame.font.SysFont(None, 40)
        self.clockFontSmall = pygame.font.SysFont(None, 40)
        self.speedFont = pygame.font.Font("fonts/mono.otf", 140)
        self.tachFont = pygame.font.Font("fonts/mono.otf", 80)
        self.fpsFont = pygame.font.SysFont(None, 20)

        self.gasPic = pygame.image.load("./gas.png")
        self.tempPic = pygame.image.load("./temp.png")
        self.gasPicPos = (20, self.screenHeight - 130)
        self.tempPicPos = (self.screenWidth - 40, self.screenHeight - 130)        
    
        # Create all labels
        self.clockHLabel = self.clockFontLarge.render(self.time, 1, WHITE)
        self.speedoLabel = self.speedFont.render(str(self.mph), 1, WHITE)
        self.tachLabel = self.tachFont.render(str(self.rpm), 1, WHITE)
        self.fpsLabel = self.fpsFont.render(str(int(self.clock.get_fps())), 1, WHITE)

        # Get rects of speed, tachRPM, clock
        self.clockHPos = self.clockHLabel.get_rect()
        self.speedoPos = self.speedoLabel.get_rect()
        self.tachPos = self.tachLabel.get_rect()
        self.fpsPos = self.fpsLabel.get_rect()

        # Posisition clock, speedo and tach
        self.clockHPos.centerx = self.screenCenterX
        self.clockHPos.y = self.screenHeight - self.clockHPos.height
        
        self.speedoPos.centerx = self.screenCenterX
        self.speedoPos.y = self.screenCenterY + 60
        
        self.tachPos.centerx = self.screenCenterX
        self.tachPos.y = self.screenCenterY - 160
        
        self.fpsPos.y = 0
        self.fpsPos.x = 0
    
    def getTachColor(self, rpmInt):
        if rpmInt < 4000:
            self.color = GREEN
        elif rpmInt >= 4000 and rpmInt < 5000:
            self.color = YELLOW
        elif rpmInt >= 5000 and rpmInt < 6100:
            self.color = ORANGE
        else:
            self.color = RED


    def getTempColor(self, tempInt):
        if tempInt < 80:
            self.color = BLUE
        elif tempInt >= 80 and tempInt < 195:
            self.color = GREEN
        elif tempInt >= 195 and tempInt < 210:
            self.color = ORANGE
        else:
            self.color = RED


    def generateScreen(self):
        self.screen.fill((0,0,0))

        # Update labels
        self.clockHLabel = self.clockFontLarge.render(self.time, 1, WHITE)
        self.speedoLabel = self.speedFont.render(str(self.mph), 1, WHITE)
        self.tachLabel = self.tachFont.render(str(self.rpm), 1, WHITE)
        self.fpsLabel = self.fpsFont.render(str(int(self.clock.get_fps())), 1, WHITE)

        # Set the color of the tach bars
        self.getTachColor(self.rpm)

        # Draw the tach bars
        barWidth = 10
        for x in range(0, self.rpm / 200):
            pygame.draw.rect(self.screen, self.color,(self.screenCenterX - 200 + (x * barWidth) + x*2, self.screenCenterY, barWidth, x * -1.5 - 35), 0)

        #draw fuel gauge
        barWidth = 70
        barHeight = 8
        for x in range(0, 10):
            pygame.draw.rect(self.screen, WHITE, (0, self.screenHeight - barHeight - (x * barHeight) - x * 1, barWidth, barHeight), 1)
        for x in range(0, self.fuel / 10):
            pygame.draw.rect(self.screen, WHITE, (0, self.screenHeight - barHeight - (x * barHeight) - x * 1, barWidth, barHeight), 0)

        # Draw temp gauge
        self.getTempColor(self.temp)
        for x in range(0, 10):
            pygame.draw.rect(self.screen, WHITE, (self.screenWidth - 70, self.screenHeight - barHeight - (x * barHeight) - x * 1, barWidth, barHeight), 1)
        for x in range(0, self.temp / 30):
            pygame.draw.rect(self.screen, self.color, (self.screenWidth - 70, self.screenHeight - barHeight - (x * barHeight) - x * 1, barWidth, barHeight), 0)


        # Blit everyting
        self.screen.blit(self.clockHLabel, self.clockHPos)
        self.screen.blit(self.speedoLabel, self.speedoPos)
        self.screen.blit(self.tachLabel, self.tachPos)
        self.screen.blit(self.fpsLabel, self.fpsPos)
        self.screen.blit(self.tempPic, self.tempPicPos)
        self.screen.blit(self.gasPic, self.gasPicPos)
        
        pygame.display.flip()
    

def main():
    loop = True
    
    refresh = 0
    rpmInt = 180
    mphInt = 100
    fuelInt = 15
    tempInt = 15
    time = "7:23"
    screen = 1
    rewind = False

    cluster = Cluster();
    
    while loop:
        for event in pygame.event.get():
            # Handle quit message received
            if event.type == pygame.QUIT:
                loop = False
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_q:
                        loop = False

        if rpmInt > 6800:
            rewind = True
        elif rpmInt < 800:
            rewind = False

        if rewind == True:
            rpmInt = rpmInt - 200
        else:
            rpmInt = rpmInt + 200

        cluster.mph = rpmInt / 60
        cluster.rpm = rpmInt
        cluster.fuel = rpmInt / 80
        cluster.temp = rpmInt / 25
        cluster.time = time
        
        cluster.generateScreen()
        
        
        # Update refresh at 10 fps
        cluster.clock.tick(20)


if __name__ == '__main__':
    main()














    
