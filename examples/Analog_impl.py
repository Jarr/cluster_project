#!/usr/bin/python

import pygame
from time import *

threadLoop = True

screenSize = (800,480)

pygame.init()

screen = pygame.display.set_mode(screenSize) #, pygame.FULLSCREEN)
clock = pygame.time.Clock()
pygame.display.set_caption("Cluster v0.02")
pygame.mouse.set_visible(False)

# Setup screen placement values
screenCenterX = screen.get_rect().centerx
screenCenterY = screen.get_rect().centery
screenHeight = screen.get_rect().height
screenWidth = screen.get_rect().width

# Setup all fonts
clockFontLarge = pygame.font.SysFont(None, 40)
clockFontSmall = pygame.font.SysFont(None, 40)
speedFont = pygame.font.SysFont(None, 100)
tachFont = pygame.font.SysFont(None, 50)
fpsFont = pygame.font.SysFont(None, 20)

gasPic = pygame.image.load("./gas.png")
tempPic = pygame.image.load("./temp.png")
facePic = pygame.image.load("./tach.png")
needlePic = pygame.image.load("./needle.png")
gasPicPos = (50, screenHeight - 120)
tempPicPos = (screenWidth - 85, screenHeight - 120)
facePicPos = (screenWidth/2 - (350/2), screenHeight - 400)



def getTachColor(rpmInt):
    if rpmInt < 4000:
        color = (0,240,0)
    elif rpmInt >= 4000 and rpmInt < 5000:
        color = (240,240,0)
    elif rpmInt >= 5000 and rpmInt < 6100:
        color = (255,85,0)
    else:
        color = (255,0,0)

    return color

def getTempColor(tempInt):
    if tempInt < 100:
        color = (0,0,240)
    elif tempInt >= 100 and tempInt < 195:
        color = (0,240,0)
    elif tempInt >= 195 and tempInt < 210:
        color = (10,240,0)
    else:
        color = (255,0,0)

    return color


def screenOne(mphInt, rpmInt, fuelInt, tempInt, time):
    screen.fill((0,0,0))
    
    #Create strings to display
    mph = str(mphInt)
    rpm = str(rpmInt)
    fps = format(clock.get_fps())

    # Create all labels
    clockHLabel = clockFontLarge.render(time, 1, (255,255,255))
    speedoLabel = speedFont.render(mph, 1, (255,255,255))
    tachLabel = tachFont.render(rpm, 1, (255,255,255))
    fpsLabel = fpsFont.render(fps, 1, (255,255,255))

    # Get rects of speed, tachRPM, clock
    clockHPos = clockHLabel.get_rect()
    speedoPos = speedoLabel.get_rect()
    tachPos = tachLabel.get_rect()
    fpsPos = fpsLabel.get_rect()

    # Posisition clock, speedo and tach
    clockHPos.centerx = screenCenterX
    clockHPos.y = screenHeight - clockHPos.height
    speedoPos.centerx = screenCenterX + 110
    speedoPos.y = screenHeight - 140
    fpsPos.y = 0
    fpsPos.x = 0

    # Draw the tach bars
    needle = pygame.transform.rotate(needlePic,rpmInt)
    needleRect = needle.get_rect()
    needleRect.centerx = facePicPos[0] + 200
    needleRect.centery = facePicPos[1] + 200

    #draw fuel gauge
    barWidth = 15
    barHeight = 85
    for x in range(0, 5):
        pygame.draw.rect(screen, (240,240,240), ((x * barWidth) + x * 2, screenHeight, barWidth, barHeight), 1)
    for x in range(0, fuelInt):
        pygame.draw.rect(screen, (240,240,240), ((x * barWidth) + x * 2, screenHeight - barHeight, barWidth, barHeight), 0)

    # Draw temp gauge
    tempColor = getTempColor(tempInt)
    for x in range(0, 5):
        pygame.draw.rect(screen, (240,240,240), (screenWidth - barWidth - (x * barWidth) - x * 2, screenHeight, barWidth, barHeight), 1)
    for x in range(0, tempInt):
        pygame.draw.rect(screen, tempColor, (screenWidth - barWidth - (x * barWidth) - x * 2, screenHeight - barHeight, barWidth, barHeight), 0)


    # Blit everyting
    screen.blit(clockHLabel, clockHPos)
    screen.blit(facePic, facePicPos)
    screen.blit(needle, needleRect)
    screen.blit(fpsLabel, fpsPos)
    screen.blit(tempPic, tempPicPos)
    screen.blit(gasPic, gasPicPos)
    screen.blit(speedoLabel, speedoPos)

    pygame.display.flip()


def main():
    loop = True
    
    refresh = 0
    rpmInt = 180
    mphInt = 100
    fuelInt = 15
    tempInt = 15
    time = "NF"
    screen = 1
    rewind = False
	
    while loop:
        for event in pygame.event.get():
            # Handle quit message received
            if event.type == pygame.QUIT:
                loop = False
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_q:
                        loop = False

        if rpmInt > 180:
            rewind = False
        elif rpmInt < -90:
            rewind = True

        if rewind == True:
            rpmInt = rpmInt + 10
        else:
            rpmInt = rpmInt - 10


        screenOne(mphInt, rpmInt, fuelInt, tempInt, time)
        
        
        # Update refresh time to 500ms in the future
        clock.tick(30)


if __name__ == '__main__':
    main()














    
