#!/usr/bin/python

import pygame
from time import *
import gps
import threading
import RPi.GPIO as GPIO

# Setup GPIO for ADC
GPIO.setmode(GPIO.BCM)
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
GPIO.setup(SPICLK, GPIO.OUT)
GPIO.setup(SPICS, GPIO.OUT)
potentiometer_adc = 0

# Setup GPS stuff
session = gps.gps("localhost", "2947")
session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
report = session.next()
threadLoop = True

# Initalize pygame stuff
pygame.init()
pygame.mouse.set_visible(False)

# Set screen and tickClock
screenSize = (800,480)
screen = pygame.display.set_mode(screenSize) #, pygame.FULLSCREEN)
clock = pygame.time.Clock()


# Setup screen placement values
screenCenterX = screen.get_rect().centerx
screenCenterY = screen.get_rect().centery
screenHeight = screen.get_rect().height
screenWidth = screen.get_rect().width

# Setup all fonts
clockFontLarge = pygame.font.SysFont(None, 40)
clockFontSmall = pygame.font.SysFont(None, 40)
speedFont = pygame.font.SysFont(None, 260)
tachFont = pygame.font.SysFont(None, 50)
fpsFont = pygame.font.SysFont(None, 20)

# Load and position pictures
gasPic = pygame.image.load("/home/pi/gas.png")
tempPic = pygame.image.load("/home/pi/temp.png")
gasPicPos = (50, screenHeight - 120)
tempPicPos = (screenWidth - 85, screenHeight - 120)

def readadc(adcnum, clockpin, mosipin, misopin, cspin):
	if ((adcnum > 7) or (adcnum < 0)):
		return -1
	GPIO.output(cspin, True)
	GPIO.output(clockpin, False)
	GPIO.output(cspin, False)
	commandout = adcnum
	commandout |= 0x18
	commandout <<= 3
	for i in range(5):
		if(commandout & 0x80):
			GPIO.output(mosipin, True)
		else:
			GPIO.output(mosipin, False)
		commandout <<= 1
		GPIO.output(clockpin, True)
		GPIO.output(clockpin, False)
	adcout = 0
	for i in range(12):
		GPIO.output(clockpin, True)
		GPIO.output(clockpin, False)
		adcout <<= 1
		if (GPIO.input(misopin)):
			adcout |= 0x1
	GPIO.output(cspin, True)
	adcout >>= 1
	return adcout

class GpsPoller(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		#gpsd = gps(mode=WATCH_ENABLE)
		self.current_value = None
		self.running = True
	def run(self):
		global report
		global session
		global threadLoop
		while threadLoop:
			report = session.next()

def getTachColor(rpmInt):
    if rpmInt < 4000:
        color = (0,240,0)
    elif rpmInt >= 4000 and rpmInt < 5000:
        color = (240,240,0)
    elif rpmInt >= 5000 and rpmInt < 6100:
        color = (255,85,0)
    else:
        color = (255,0,0)

    return color

def getTempColor(tempInt):
    if tempInt < 100:
        color = (0,0,240)
    elif tempInt >= 100 and tempInt < 195:
        color = (0,240,0)
    elif tempInt >= 195 and tempInt < 210:
        color = (10,240,0)
    else:
        color = (255,0,0)

    return color


def generateScreen(mphInt, rpmInt, fuelInt, tempInt, time):
    screen.fill((0,0,0))
    
    #Create strings to display
    mph = str(mphInt)
    rpm = str(rpmInt)
    fps = str(int(clock.get_fps()))

    # Create all labels
    clockHLabel = clockFontLarge.render(time, 1, (255,255,255))
    speedoLabel = speedFont.render(mph, 1, (255,255,255))
    tachLabel = tachFont.render(rpm, 1, (255,255,255))
    fpsLabel = fpsFont.render(fps, 1, (255,255,255))

    # Get rects of speed, tachRPM, clock
    clockHPos = clockHLabel.get_rect()
    speedoPos = speedoLabel.get_rect()
    tachPos = tachLabel.get_rect()
    fpsPos = fpsLabel.get_rect()

    # Posisition clock, speedo and tach
    clockHPos.centerx = screenCenterX
    clockHPos.y = screenHeight - clockHPos.height
    speedoPos.centerx = screenCenterX
    speedoPos.y = screenCenterY - 25
    tachPos.centerx = screenCenterX
    tachPos.y = screenCenterY - 200
    fpsPos.y = 0
    fpsPos.x = 0

    # Set the color of the tach bars
    tachColor = getTachColor(rpmInt)

    # Draw the tach bars
    barWidth = 10
    for x in range(0, rpmInt / 200):
        pygame.draw.rect(screen, tachColor,(screenCenterX - 200 + (x * barWidth) + x*2, screenCenterY - 70, barWidth, x * -1.5 - 35), 0)

    #draw fuel gauge
    barWidth = 35
    barHeight = 15
    for x in range(0, 5):
        pygame.draw.rect(screen, (240,240,240), (50, screenHeight - barHeight - (x * barHeight) - x * 2, barWidth, barHeight), 1)
    for x in range(0, fuelInt / 20):
        pygame.draw.rect(screen, (240,240,240), (50, screenHeight - barHeight - (x * barHeight) - x * 2, barWidth, barHeight), 0)

    # Draw temp gauge
    tempColor = getTempColor(tempInt)
    for x in range(0, 5):
        pygame.draw.rect(screen, (240,240,240), (screenWidth - 85, screenHeight - barHeight - (x * barHeight) - x * 2, barWidth, barHeight), 1)
    for x in range(0, tempInt / 50):
        pygame.draw.rect(screen, tempColor, (screenWidth - 85, screenHeight - barHeight - (x * barHeight) - x * 2, barWidth, barHeight), 0)


    # Blit everyting
    screen.blit(clockHLabel, clockHPos)
    screen.blit(speedoLabel, speedoPos)
    screen.blit(tachLabel, tachPos)
    screen.blit(fpsLabel, fpsPos)
    screen.blit(tempPic, tempPicPos)
    screen.blit(gasPic, gasPicPos)
    
    pygame.display.flip()
    

def main():
    loop = True
    global threadLoop

    refresh = 0
    rpmInt = 5000
    mphInt = "NF"
    fuelInt = 55
    tempInt = 290
    time = "NF"
	
    global report
    global session
    gpsp = GpsPoller()
    gpsp.start()    

    while loop:
        for event in pygame.event.get():
            # Handle quit message received
            if event.type == pygame.QUIT:
                loop = False
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_q:
		    threadLoop = False
		    gpsp.join()
		    loop = False
                elif event.key == pygame.K_1:
                    screen = 1
                elif event.key == pygame.K_2:
                    screen = 2
            
        # Update speed and RPM and clock and stuff
	if hasattr(report, 'speed'):
		mphInt = report.speed
		mphInt = int(mphInt * 2.23693)
	if hasattr(report, 'time'):
		time = report.time.split('T')[1].split('.')[0].split(':')
		timeH = str( (((int(time[0]) + 11) % 12) + 1) - 7 + 12)
		timeM = time[1]
		time = timeH + ':' + timeM            
        rpmInt = 6 * readadc(potentiometer_adc, SPICLK, SPIMOSI, SPIMISO, SPICS)      
	fuelInt = readadc(potentiometer_adc, SPICLK, SPIMOSI, SPIMISO, SPICS) / 10
	tempInt = readadc(potentiometer_adc, SPICLK, SPIMOSI, SPIMISO, SPICS) / 5       

	# Run the function to update display
        generateScreen(mphInt, rpmInt, fuelInt, tempInt, time)
        
        # refresh at 10 FPS
        clock.tick(10)


if __name__ == '__main__':
    main()














    
