#include <SPI.h>
#include "mcp_can.h"

const int SPI_CS_PIN = 10;
const int ANAL_PIN = 0;
const int Vin = 5;
const float R1 = 200;

unsigned char len = 0;
unsigned char CANbuff[8];
unsigned int data[7];
int a_in = 0;
int Vout = 0;
unsigned long secs = 500;

MCP_CAN CAN(SPI_CS_PIN);

void setup()
{
    Serial.begin(115200);

    while (CAN_OK != CAN.begin(CAN_1000KBPS)) 
    {
        Serial.println("CAN BUS Shield init fail");
        Serial.println(" Init CAN BUS Shield again");
        delay(100);
    }
    Serial.println("CAN BUS Shield init ok!");
}


void loop()
{
    if(millis() - secs > 500){
        a_in = analogRead(ANAL_PIN);
        
        if(a_in)
        {
            Vout = int(((a_in * Vin) / 1024.0) * 100);
            secs = millis();
        }
    }
    if(CAN_MSGAVAIL == CAN.checkReceive()) 
    {
      CAN.readMsgBuf(&len, CANbuff);
      unsigned int canId = CAN.getCanId();
      
      switch (canId){
        case 0x360:
          data[0] = 0;
          //RPM
          data[1] = CANbuff[0];
          data[2] = CANbuff[1];
          //Manifold pressure
          data[3] = CANbuff[2];
          data[4] = CANbuff[3];
          //TPS
          data[5] = CANbuff[4];
          data[6] = CANbuff[5];
        break;
  
        case 0x368:
          data[0] = 1;
          //Lambda 1
          data[1] = CANbuff[0];
          data[2] = CANbuff[1];
          //Fuel level (FROM ANALOG INPUT)
          data[3] = highByte(Vout);
          data[4] = lowByte(Vout);
          //NA
          data[5] = 0; data[6] = 0;	  	
        break;
  
        case 0x369:
          data[0] = 2;
      	  //Miss count
      	  data[1] = CANbuff[0];
          data[2] = CANbuff[1];
      	  //NA	  	
      	  data[3] = 0; data[4] = 0; data[5] = 0; data[6] = 0;
        break;
  
        case 0x370:
          data[0] = 3;
          //speed
          data[1] = CANbuff[0];
          data[2] = CANbuff[1];
          //Gear	  	
          data[3] = CANbuff[2];
          data[4] = CANbuff[3];	  	
          //NA
          data[5] = 0; data[6] = 0;
        break;
  
        case 0x372:
          data[0] = 4;
      	  //Battery voltage
      	  data[1] = CANbuff[0];
          data[2] = CANbuff[1];
      	  //NA
      	  data[3] = 0; data[4] = 0; data[5] = 0; data[6] = 0;	
        break;
  
        case 0x3E0:
          data[0] = 5;
      	  //Coolant temp
      	  data[1] = CANbuff[0];
          data[2] = CANbuff[1];	  	
      	  //IAT
      	  data[3] = CANbuff[2];
          data[4] = CANbuff[3];
      	  //NA
      	  data[5] = 0; data[6] = 0;	
        break;
  
        case 0x3E4:
          data[0] = 6;
          //Rally antilag
          data[1] = bitRead(CANbuff[2],3);	  	
          //CEL
          data[2] = bitRead(CANbuff[7],0);
          //NA
          data[3] = 0; data[4] = 0; data[5] = 0; data[6] = 0;
        break;
  
        default:
        break;
        }

        for(int i = 0; i < 7; i++){
          Serial.write(data[i]);
        }
    }
}
