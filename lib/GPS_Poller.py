import gps
import threading

class GPS_Poller(threading.Thread):
    def __init__(self):
        self.gpsd = gps.gps("localhost", "2947")
        self.gpsd.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
        self.report = self.gpsd.next()
        self.threadLoop = True
        self.current_value = None
        self.running = True
        threading.Thread.__init__(self)

    def run(self):
        while self.threadLoop:
            self.report = self.gpsd.next()

