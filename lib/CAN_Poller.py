import serial
import threading

class CAN_Poller(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.threadLoop = True
        self.rpm = 0
        self.boost = 0
        self.tps = 0
        self.afr = 0.0
        self.fuel = 0
        self.missCount = 0
        self.speed = 0
        self.gear = 0
        self.voltage = 0.0
        self.coolantTmp = 0
        self.iat = 0
        self.antilag = 0
        self.mil = 0
        self.readError = False
        self.syncError = False
	
        try:
            self.connection = serial.Serial('/dev/ttyACM0', 115200, timeout=0.5)
        except:
            try:
                self.connection = serial.Serial('/dev/ttyACM1', 115200, timeout=0.5)
            except:
                self.connection = None
        	self.readError = 2
        if self.connection != None:
        	self.connection.flush()


    def run(self):
        data = [7]
        while self.threadLoop and self.connection != None:
            data = [0,0,0,0,0,0,0]
            datastring = self.connection.read(7)
            if len(datastring) != 7:
                self.readError = 1
            else:
                self.readError = False
                self.syncError = False
                data = map(ord, datastring)
                if data[0] == 0:
                    self.rpm = (data[1] * 256 + data[2])
                    self.boost = ((data[3] * 256 + data[4]) * 0.1) / 100
                    self.tps = (data[5] * 256 + data[6]) * 0.1

                elif data[0] == 1:
                    self.afr = (data[1] * 256 + data[2]) * 0.001
                    fuelVolt = (data[3] * 256 + data[4])
                    self.fuel = (((fuelVolt - 339) * (100)) / (410 - 339))

                elif data[0] == 2:
                    self.missCount = data[1] * 256 + data[2]

                elif data[0] == 3:
                    self.speed = ((data[1] * 256 + data[2]) * 0.1) * 0.62137
                    self.gear = data[3] * 256 + data[4]

                elif data[0] == 4:
                    self.voltage = (data[1] * 256 + data[2]) * 0.1

                elif data[0] == 5:
                    self.coolantTmp = ((data[1] * 256 + data[2]) * 0.1) - 273.15
                    self.iat = ((data[3] * 256 + data[4]) * 0.1) - 273.15

                elif data[0] == 6:
                    self.antilag = data[1]
                    self.mil = data[2]			

                else:
                    self.syncError = True
                    self.connection.read(1)

