import pygame
import math

WHITE = (255, 255, 255)
GREEN = (76, 175, 80)
RED = (244, 67, 54)
YELLOW = (255, 152, 0)
ORANGE = (255, 87, 34)
BLUE = (22, 150, 243)
BGCOLOR = (125, 125, 125)

class Cluster():
    def __init__(self):
        pygame.init()
        
        self.screenSize = (800,480)
        #self.color = (0,0,0)
        self.fps = 0

        self.speed = 000
        self.rpm = 0000
        self.coolantTmp = 000
        self.fuel = 000
        self.gear = 0
        self.boost = 0
        self.boostTgt = 0
        self.tps = 000
        self.afr = 00.0
        self.voltage = 00.0
        self.mil = 0
        self.ral = 0

        self.info = "Loading"

        self.screen = pygame.display.set_mode(self.screenSize)
        pygame.mouse.set_visible(True)

        self.clock = pygame.time.Clock()

    #------ Setup screen placement values ------#
        self.screenCenterX = self.screen.get_rect().centerx
        self.screenCenterY = self.screen.get_rect().centery
        self.screenHeight = self.screen.get_rect().height
        self.screenWidth = self.screen.get_rect().width

    #------ Load and position images ------#
        self.bgPic = pygame.image.load("./img/cluster.png")
        self.bgPicPosx = 0
        self.bgPicPosy = 0

        self.tachPicB = pygame.image.load("./img/tachB.png")
        self.tachPicO = pygame.image.load("./img/tachO.png")
        self.tachPicR = pygame.image.load("./img/tachR.png")
        self.tachPicPosX = 0
        self.tachPicPosY = -185

        self.boostPicB = pygame.image.load("./img/boostB.jpg")
        self.boostPicG = pygame.image.load("./img/boostG.jpg")
        self.boostPicR = pygame.image.load("./img/boostR.jpg")
        self.boostPicPosX = -199
        self.boostPicPosY = 230

        self.voltageLowPic = pygame.image.load("./img/voltLow.png")
        self.voltageLowPosX = 250
        self.voltageLowPosY = 325
        
        self.milPic = pygame.image.load("./img/mil.png")
        self.milPosX = 500
        self.milPosY = 325
        
    #------ Setup all fonts ------#
        self.font30 = pygame.font.SysFont(None, 30)
        self.font60 = pygame.font.Font(pygame.font.match_font('Verdana', bold = True),60)
        self.font80 = pygame.font.Font(pygame.font.match_font('Verdana', bold = True),80)
        self.font90 = pygame.font.Font(pygame.font.match_font('Verdana', bold = True),90)
    
    #------ Create all labels ------#
        self.infoLabel = self.font30.render(self.info, 1, WHITE)
        self.speedoLabel = self.font90.render(str(self.speed), 1, WHITE)
        self.tachLabel = self.font60.render(str(self.rpm), 1, WHITE)
        self.coolantTmpLabel = self.font80.render(str(self.coolantTmp), 1, WHITE)
        self.gearLabel = self.font90.render(str(self.gear), 1, WHITE)
        self.fuelLabel = self.font80.render(str(self.fuel), 1, WHITE)
        self.fpsLabel = self.font30.render(str(int(self.clock.get_fps())), 1, WHITE)

    #------ Get rects of labels ------#
        self.infoPos = self.infoLabel.get_rect()
        self.speedoPos = self.speedoLabel.get_rect()
        self.tachPos = self.tachLabel.get_rect()
        self.coolantTmpPos = self.coolantTmpLabel.get_rect()
        self.gearPos = self.gearLabel.get_rect()
        self.fuelPos = self.fuelLabel.get_rect()
        self.fpsPos = self.fpsLabel.get_rect()
        
    #------ Position simple number things ------#
        self.infoPos.x = 0
        self.infoPos.y = self.screenHeight - self.infoPos.height

        self.speedoPos.centerx = self.screenCenterX
        self.speedoPos.y = self.screenCenterY + 110
        
        self.tachPos.centerx = self.screenCenterX
        self.tachPos.y = self.screenCenterY - 100

        self.gearPos.centerx = self.screenCenterX
        self.gearPos.centery = self.screenCenterY + 50

        self.fuelPos.centerx = self.screenCenterX + 300
        self.fuelPos.y = self.screenCenterY + 110

        self.coolantTmpPos.centerx = self.screenCenterX + 300
        self.coolantTmpPos.y = self.screenCenterY + 20
        
        self.fpsPos.y = 0
        self.fpsPos.x = 0
        
    #------ Default the numbers ------#
        self.speed = 0
        self.rpm = 0
    
    def rotateTach(self):
        # NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
        angle = NewValue = (((self.rpm - 0) * (180 - 0)) / (8500 - 0)) + 0
        if angle > 150:
            image = self.tachPicR
        elif angle < 130 and angle > 151:
            image = self.tachPicO
        else:
            image = self.tachPicB

        orig_rect = image.get_rect()
        rot_image = pygame.transform.rotate(image, -angle)
        rot_rect = orig_rect.copy() 
        rot_rect.center = rot_image.get_rect().center
        rot_image = rot_image.subsurface(rot_rect).copy()
        
        return rot_image

    def boostPos(self):
        x = (((self.boost - 0) * (10 - (-199))) / (1.4 - 0)) + (-199)
        xadj = x + 208
        if xadj > 181:
            return self.boostPicR, x
        elif xadj < 181 and xadj > 55:
            return self.boostPicG, x
        else:
            return self.boostPicB, x

    def checkEngine(self):
        if self.voltage < 9.0:
            self.screen.blit(self.voltageLowPic, (self.voltageLowPosX, self.voltageLowPosY))
        if self.mil == 1:
            self.screen.blit(self.milPic, (self.milPosX, self.milPosY))

    def generateScreen(self):
        self.screen.fill(BGCOLOR)

    #------ blit tach and boost pics first so they are under the BG pic ------#
        tachPic = self.rotateTach()
        self.screen.blit(tachPic, (self.tachPicPosX, self.tachPicPosY))

        boostPic, self.boostPicPosX = self.boostPos()
        pygame.draw.rect(self.screen, BGCOLOR, (0,225, 225, 80), 0)
        self.screen.blit(boostPic, (self.boostPicPosX, self.boostPicPosY))

        self.screen.blit(self.bgPic, (self.bgPicPosx, self.bgPicPosy))

        self.checkEngine()

    #------ Realign the speedo since it may have changed significantly ------#
        self.speedoPos = self.speedoLabel.get_rect()
        self.speedoPos.centerx = self.screenCenterX
        self.speedoPos.y = self.screenCenterY + 110

    #------ Update labels ------#
        self.infoLabel = self.font30.render(self.info, 1, WHITE)
        self.speedoLabel = self.font90.render(str(int(self.speed)), 1, WHITE)
        self.tachLabel = self.font60.render(str(self.rpm), 1, WHITE)
        self.coolantTmpLabel = self.font80.render(str(int(self.coolantTmp)), 1, WHITE)
        self.gearLabel = self.font80.render(str(self.gear), 1, WHITE)
        self.fuelLabel = self.font80.render(str(self.fuel), 1, WHITE)
        self.fpsLabel = self.font30.render(str(int(self.clock.get_fps())), 1, WHITE)

    #------ Blit everyting ------#
        
        self.screen.blit(self.infoLabel, self.infoPos)
        self.screen.blit(self.speedoLabel, self.speedoPos)
        self.screen.blit(self.tachLabel, self.tachPos)
        self.screen.blit(self.coolantTmpLabel, self.coolantTmpPos)
        self.screen.blit(self.gearLabel, self.gearPos)
        self.screen.blit(self.fuelLabel, self.fuelPos)
        self.screen.blit(self.fpsLabel, self.fpsPos)

        pygame.display.update()
