#!/usr/bin/env python
############################################################################
#
# wxgui.py
#
# Copyright 2004 Donour Sizemore (donour@uchicago.edu)
# Copyright 2009 Secons Ltd. (www.obdtester.com)
#
# This file is part of pyOBD.
#
# pyOBD is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyOBD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyOBD; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
############################################################################
# adapted to maemo5/n900 by emme750 + improvement by vanous and GizmoXomziG
# pyobd-0.9.2-28
############################################################################

#import wxversion
#wxversion.select("2.6")
import wx
    
import obd_io #OBD2 funcs
import os #os.environ

import SpeedMeter as SM
from math import pi, sqrt

import threading
import sys
import serial
import platform
import time
import datetime
import ConfigParser #safe application configuration
#import webbrowser #open browser from python

from obd2_codes import pcodes
from obd2_codes import ptest


#import sys
import dbus
from bluetooth import *
import hildon

from wx.lib.mixins.listctrl import ListCtrlAutoWidthMixin

_fromUtf8 = lambda s: s

ID_ABOUT       = 101
ID_EXIT        = 110
ID_FULLSCREEN  = 111
ID_GAUGES      = 112
ID_CSVLOG      = 113
ID_CONFIG      = 500
ID_CLEAR       = 501
ID_GETC        = 502
ID_RESET       = 503
ID_LOOK        = 504
ALL_ON         = 505
ALL_OFF        = 506

ID_DISCONNECT  = 507
ID_HELP_ABOUT  = 508
ID_HELP_VISIT  = 509
ID_HELP_ORDER  = 510
ID_HELP_ABOUT_Maemo = 511
ID_HELP_VISIT_Maemo = 512

SX = 0
CSVLogEnabled = 0

# Define notification event for sensor result window
EVT_RESULT_ID = 1000
def EVT_RESULT(win, func,id):
    """Define Result Event."""
    win.Connect(-1, -1, id, func)
        
#event pro akutalizaci Trace tabu
class ResultEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""
    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RESULT_ID)
        self.data = data

#event pro aktualizaci DTC tabu
EVT_DTC_ID = 1001        
class DTCEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""
    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_DTC_ID)
        self.data = data

#event pro aktualizaci status tabu
EVT_STATUS_ID = 1002        
class StatusEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""
    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_STATUS_ID)
        self.data = data

#event pro aktualizaci tests tabu
EVT_TESTS_ID = 1003        
class TestEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""
    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_TESTS_ID)
        self.data = data

# Define notification event for dial result window
EVT_DIAL_ID = 1004
class DialEvent(wx.PyEvent):
    def __init__(self, data):
        """Init Dial Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_DIAL_ID)
        self.data = data

EVT_DIAL1_ID = 1005

class DialEvent1(wx.PyEvent):
    def __init__(self, data):
        """Init Dial1 Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_DIAL1_ID)
        self.data = data

        
#defines notification event for debug tracewindow
from debugEvent import *
        
class MyApp(wx.App):
    # A listctrl which auto-resizes the column boxes to fill

    class MyListCtrl(wx.ListCtrl, ListCtrlAutoWidthMixin):
        def __init__(self, parent, id, pos = wx.DefaultPosition, size = wx.DefaultSize, style = 0):

            wx.ListCtrl.__init__(self,parent,id,pos,size,style)
            ListCtrlAutoWidthMixin.__init__(self)

    class sensorProducer(threading.Thread):
        def __init__(self, _notify_window,portName,SERTIMEOUT,RECONNATTEMPTS,_nb):

            from Queue import Queue
            self.portName = portName
            self.RECONNATTEMPTS=RECONNATTEMPTS
            self.SERTIMEOUT=SERTIMEOUT 
            self.port = None
            self._notify_window=_notify_window
            self._nb=_nb
            threading.Thread.__init__ ( self )

        def initCommunication(self):

            self.port = obd_io.OBDPort(self.portName,self._notify_window,self.SERTIMEOUT,self.RECONNATTEMPTS)

            if self.port.State==0: #Cant open serial port
                return None

            self.active = []
            self.refreshing = []

            self.supp    = self.port.sensor(0)[1] #read supported PIDS1x
            self.supp1    = self.port.sensor(32)[1] #read supported PIDS2x
            self.supp2    = self.port.sensor(64)[1] #read supported PIDS3x

            self.supp = str(self.supp+self.supp1+self.supp2)

            print "self.sup f: " + self.supp

            self.active.append(1) #PID 0 is always supported
            self.refreshing.append(0) #but is not being refreshed
         
            wx.PostEvent(self._notify_window, ResultEvent([0,0,"X"]))
            wx.PostEvent(self._notify_window, ResultEvent([0,1,"X"]))
            wx.PostEvent(self._notify_window, DebugEvent([1,"Communication initialized..."]))

            for i in range(1, len(self.supp)):
                if self.supp[i-1] == "1": #put X in coloum if PID is supported
                    self.active.append(1)
                    self.refreshing.append(1)
                    wx.PostEvent(self._notify_window, ResultEvent([i,0,"X"]))
                    wx.PostEvent(self._notify_window, ResultEvent([i,1,"X"]))
                else:
                    self.active.append(0)
                    self.refreshing.append(0)
                    wx.PostEvent(self._notify_window, ResultEvent([i,0,""]))
                    wx.PostEvent(self._notify_window, ResultEvent([i,1,""]))

            return "OK"
        

        def run(self):
            global btname, maxbar, maxmaf
            maxbar=0
            maxmaf=0
            global SX
            global CSVLogEnabled
            
            wx.PostEvent(self._notify_window, StatusEvent([0,1,"Connecting...."]))
            try:
                self.initCommunication()
            except:
                pass
            try:
                if self.port.State==0: #cant connect, exit thread
                    self.stop()
                    wx.PostEvent(self._notify_window, StatusEvent([666])) #signal apl, that communication was disconnected
                    wx.PostEvent(self._notify_window, StatusEvent([0,1,"Error cant connect..."]))
                    return None
            except:
                wx.PostEvent(self._notify_window, StatusEvent([0,1,"Disconnected"]))
                return None
                
            wx.PostEvent(self._notify_window, StatusEvent([0,1,"Connected"]))
            wx.PostEvent(self._notify_window, StatusEvent([3,1,self.portName]))
            wx.PostEvent(self._notify_window, StatusEvent([4,1,btname]))

            os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"Connected to ECU"')

            sensLogFile="/home/user/MyDocs/pyobd-"+str(datetime.datetime.strftime(datetime.datetime.now(),"%Y%m%d-%H%M%S"))+".csv"
            sensLogStarted=0
            
            log2="echo Connected > /home/user/MyDocs/pyobd.log"
            os.popen(log2)
            log2="echo >> /home/user/MyDocs/pyobd.log"
            os.popen(log2)

            wx.PostEvent(self._notify_window, StatusEvent([2,1,self.port.ELMver]))
            prevstate=-1
            curstate=-1

            while self._notify_window.ThreadControl!=666:
                #wx.Yield()
                prevstate=curstate
                curstate=self._nb.GetSelection()
                #print "curstate: "+str(curstate)
                time.sleep(0.1)

                if curstate==0: #show status tab
                    pass
                elif curstate==1: #show tests tab
                    res=self.port.get_tests_MIL()                
                    for i in range(0,len(res)):
                        wx.PostEvent(self._notify_window, TestEvent([i,1,res[i]]))
                
                elif curstate==2: #show sensor tab

                    CSVLogEnabledNow=CSVLogEnabled
                    sensLogLine=""
                    if CSVLogEnabledNow == 1:
                        if sensLogStarted==0: #first time = header and notification
                            os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"Started logging sensor values into '+sensLogFile+'"')
                            sensLogLine="Date;Time"
                        else:
                            sensLogLine=str(datetime.datetime.strftime(datetime.datetime.now(),"%d.%m.%Y"))+";"+str(datetime.datetime.strftime(datetime.datetime.now(),"%H:%M:%S"))
                    
                    log2="echo    >> /home/user/MyDocs/pyobd.log"
                    os.popen(log2)
                    log1="time : "+str(datetime.datetime.now())
                    log2="echo "+log1+" >> /home/user/MyDocs/pyobd.log"
                    os.popen(log2)
                    log2="echo    >> /home/user/MyDocs/pyobd.log"
                    os.popen(log2)

                    for i in range(0, len(self.active)):

                        if self.refreshing[i]:
                            s = self.port.sensor(i)
                            
                            print s[1][0],s[1][1],s[2],s[3]
                            wx.PostEvent(self._notify_window, ResultEvent([i,3,"%s (%s)" % (s[1][SX], s[2+SX])]))
#log
                            if CSVLogEnabledNow == 1:
                                if sensLogStarted==0: #first time = header
                                    sensLogLine+=";"+str(s[0])+" ("+str(s[2+SX])+")"
                                else:
                                    sensLogLine+=";"+str(s[1][SX])
                            
                            log1=str(s[0])+" : "+str(s[1][SX])+" "+str(s[2+SX])
                            #print s
                            print log1
                            log2="echo "+log1+" >> /home/user/MyDocs/pyobd.log"
                            os.popen(log2)
                            log2="echo    >> /home/user/MyDocs/pyobd.log"
                            os.popen(log2)
                            
                        elif self.active[i]:
                            if CSVLogEnabledNow == 1:
                                if sensLogStarted==0: #first time = header
                                    s = self.port.sensor(i)
                                    sensLogLine+=";"+str(s[0])+" ("+str(s[2+SX])+")" #only need the sensor name (this can probably be done better)
                                else:
                                    sensLogLine+=";"

                        if self._notify_window.ThreadControl==666:
                            break
                    
                    if CSVLogEnabledNow == 1:
                        log2="echo \""+sensLogLine+"\" >> "+sensLogFile
                        os.popen(log2)
                        sensLogStarted=1
                
                elif curstate==3: #show DTC tab
                    if self._notify_window.ThreadControl == 1: #clear DTC
                        self.port.clear_dtc()

                        if self._notify_window.ThreadControl==666: #before reset ThreadControl we must check if main thread did not want us to finish
                            break
                            
                        self._notify_window.ThreadControl=0
                        prevstate=-1 # to reread DTC
                    if self._notify_window.ThreadControl == 2: #reread DTC
                        prevstate=-1
                        
                        if self._notify_window.ThreadControl==666:
                            break
                            
                        self._notify_window.ThreadControl=0
                    if prevstate!=3: 
                        wx.PostEvent(self._notify_window, DTCEvent(0)) #clear list
                        DTCCodes=self.port.get_dtc()

                        if len(DTCCodes)==0:
                            wx.PostEvent(self._notify_window, DTCEvent(["","","No DTC codes (codes cleared)"]))
                            log1="Value :    No DTC codes or codes cleared"
                            print log1
                            log2="echo "+log1+" >> /home/user/MyDocs/pyobd.log"
                            os.popen(log2)
                            log2="echo    >> /home/user/MyDocs/pyobd.log"
                            os.popen(log2)

                        for i in range (0,len(DTCCodes)):
                            try:
                                wx.PostEvent(self._notify_window, DTCEvent([DTCCodes[i][1],DTCCodes[i][0],pcodes[DTCCodes[i][1]]]))
                                log1="Value :    "+str(DTCCodes[i][1])+" "+str(DTCCodes[i][0])+" "+str(pcodes[DTCCodes[i][1]])
                                print log1
                                log2="echo '"+log1+"' >> /home/user/MyDocs/pyobd.log"
                                os.popen(log2)
                                log2="echo    >> /home/user/MyDocs/pyobd.log"
                                os.popen(log2)
                            except:
                                wx.PostEvent(self._notify_window, DTCEvent([DTCCodes[i][1],DTCCodes[i][0],"? UNKNOWN"]))
                                log1="Value :    "+str(DTCCodes[i][1])+" "+str(DTCCodes[i][0])+" ? UNKNOWN"
                                print log1
                                log2="echo "+log1+" >> /home/user/MyDocs/pyobd.log"
                                os.popen(log2)
                                log2="echo    >> /home/user/MyDocs/pyobd.log"
                                os.popen(log2)
                                pass

#Gauges
                elif curstate==4:

                    speedsensor=13
                    if self.active[speedsensor]:
                        s = self.port.sensor(speedsensor)
                        text=str(s[1][SX])
                        text="spe"+text
                        wx.PostEvent(self._notify_window, DialEvent(text))

                    speedsensor=12
                    if self.active[speedsensor]:
                        s = self.port.sensor(speedsensor)
                        text="rpm"+str(s[1][0])
                        wx.PostEvent(self._notify_window, DialEvent(text))
                    wx.Yield()

                elif curstate==5:

                    speedsensor=11 # MAP
                    if self.active[speedsensor]:
                        s = self.port.sensor(speedsensor)
                        text=str(s[1][0])
                        text="MAP"+text
                        print text
                        wx.PostEvent(self._notify_window, DialEvent1(text))

                    speedsensor=16 # MAF
                    if self.active[speedsensor]:
                        s = self.port.sensor(speedsensor)
                        text=str(s[1][SX])
                        text="MAF"+text
                        print text
                        wx.PostEvent(self._notify_window, DialEvent1(text))
                    wx.Yield()                    


                else:
                    pass 
            self.stop()


        def off(self, id):

            if id >= 0 and id < len(self.refreshing): 
                self.refreshing[id] = 0
            else:
                debug("Invalid sensor id")


        def on(self, id):

            if id >= 0 and id < len(self.refreshing): 
                self.refreshing[id] = 1
            else:
                debug("Invalid sensor id")

        def all_off(self):

            for i in range(0, len(self.refreshing)):
                self.off(i)


        def all_on(self):

            for i in range(0, len(self.refreshing)):
                self.on(i)
                
        def stop(self):
            if self.port != None: #if stop is called before any connection port is not defined (and not connected )
                try:
                    self.port.close()
                except:
                    pass

            wx.PostEvent(self._notify_window, StatusEvent([0,1,"Disconnected"]))
            wx.PostEvent(self._notify_window, StatusEvent([2,1,"----"]))
    
    #class producer end
        
    def sensor_control_on(self): #after connection enable few buttons
        self.settingmenu.Enable(ID_CONFIG,False)
        self.settingmenu.Enable(ID_RESET,False)
        self.settingmenu.Enable(ID_DISCONNECT,True)
        self.dtcmenu.Enable(ID_GETC,True)
        self.dtcmenu.Enable(ID_CLEAR,True)
        self.GetDTCButton.Enable(True)
        self.ClearDTCButton.Enable(True)

        os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"Trying to connecting to ECU, wait some seconds..."')

        def sensor_toggle(e):
            sel = e.m_itemIndex
            state = self.senprod.refreshing[sel]
            print sel, state
            if state == 0:
                if self.senprod.active[sel] == 1:
                    self.senprod.on(sel)
                    self.sensors.SetStringItem(sel,1,"X")
            elif state == 1:
                self.senprod.off(sel)
                self.sensors.SetStringItem(sel,1,"")
            else:
                debug("Incorrect sensor state")
        
        self.sensors.Bind(wx.EVT_LIST_ITEM_ACTIVATED,sensor_toggle,id=self.sensor_id)                

    def sensor_control_off(self): #after disconnect disable fer buttons
        self.dtcmenu.Enable(ID_GETC,False)
        self.dtcmenu.Enable(ID_CLEAR,False)
        self.settingmenu.Enable(ID_DISCONNECT,False)
        self.settingmenu.Enable(ID_CONFIG,True)
        self.settingmenu.Enable(ID_RESET,True)
        self.GetDTCButton.Enable(False)
        self.ClearDTCButton.Enable(False)
        #http://pyserial.sourceforge.net/                                                    empty function
        #EVT_LIST_ITEM_ACTIVATED(self.sensors,self.sensor_id, lambda : None)
                
    def build_sensor_page(self):
        HOFFSET_LIST=0
        tID = wx.NewId()
        self.sensor_id = tID
        panel = wx.Panel(self.nb, -1)
     
        self.sensors = self.MyListCtrl(panel, tID, pos=wx.Point(0,HOFFSET_LIST),
                                    style=
                                    wx.LC_REPORT     |    
                                    wx.SUNKEN_BORDER |
                                    wx.LC_HRULES     |
                                    wx.LC_SINGLE_SEL)

        self.sensors.InsertColumn(0, "S.",width=40)
        self.sensors.InsertColumn(1, "R.",width=40)
        self.sensors.InsertColumn(2, "Sensor",format=wx.LIST_FORMAT_CENTER, width=400)
        self.sensors.InsertColumn(3, "Value")

        for i in range(0, len(obd_io.obd_sensors.SENSORS)):
            s = obd_io.obd_sensors.SENSORS[i].name
            self.sensors.InsertStringItem(i, "")
            self.sensors.SetStringItem(i, 2, s)
            
        
        ####################################################################
        # This little bit of magic keeps the list the same size as the frame
        def OnPSize(e, win = panel):
            panel.SetSize(e.GetSize())
            self.sensors.SetSize(e.GetSize())
            w,h = self.frame.GetClientSizeTuple()
            self.sensors.SetDimensions(0,HOFFSET_LIST, w-10 , h - 35 )

        panel.Bind(wx.EVT_SIZE,OnPSize)
        ####################################################################

        self.nb.AddPage(panel, "Sensors")
    
    def build_DTC_page(self):
        HOFFSET_LIST=40 #offset from the top of panel (space for buttons)
        tID = wx.NewId()
        self.DTCpanel = wx.Panel(self.nb, -1)
        self.GetDTCButton    = wx.Button(self.DTCpanel,-1 ,"Get DTC" , wx.Point(15,0))
        self.ClearDTCButton = wx.Button(self.DTCpanel,-1,"Clear DTC", wx.Point(150,0))
        
        #bind functions to button click action
        self.DTCpanel.Bind(wx.EVT_BUTTON,self.GetDTC,self.GetDTCButton)
        self.DTCpanel.Bind(wx.EVT_BUTTON,self.QueryClear,self.ClearDTCButton)
        
        self.dtc = self.MyListCtrl(self.DTCpanel,tID, pos=wx.Point(0,HOFFSET_LIST),
                            style=wx.LC_REPORT|wx.SUNKEN_BORDER|wx.LC_HRULES|wx.LC_SINGLE_SEL)
                                    
        self.dtc.InsertColumn(0, "Code", width=100)
        self.dtc.InsertColumn(1, "Status",width=100)
        self.dtc.InsertColumn(2, "Trouble code")
        ####################################################################
        # This little bit of magic keeps the list the same size as the frame
        def OnPSize(e, win = self.DTCpanel):
            self.DTCpanel.SetSize(e.GetSize())
            self.dtc.SetSize(e.GetSize())
            w,h = self.frame.GetClientSizeTuple()
            # I have no idea where 70 comes from
            self.dtc.SetDimensions(0,HOFFSET_LIST, w-16 , h - 70 )

        self.DTCpanel.Bind(wx.EVT_SIZE,OnPSize)
        ####################################################################
        
        self.nb.AddPage(self.DTCpanel, "DTC")


    def build_dial_page(self):
        HOFFSET_LIST=0 #offset from the top of panel (space for buttons)
        Dialpanel = wx.Panel(self.nb, -1)
        self.Dialpanel=Dialpanel
        tID = wx.NewId()
        self.dial_id = tID

        # First SpeedMeter: We Use The Following Styles:
        #
        # SM_DRAW_HAND: We Want To Draw The Hand (Arrow) Indicator
        # SM_DRAW_SECTORS: Full Sectors Will Be Drawn, To Indicate Different Intervals
        # SM_DRAW_MIDDLE_TEXT: We Draw Some Text In The Center Of SpeedMeter
        # SM_DRAW_SECONDARY_TICKS: We Draw Secondary (Intermediate) Ticks Between
        #                            The Main Ticks (Intervals)

        self.SpeedWindow1 = SM.SpeedMeter(Dialpanel,
                                            extrastyle=SM.SM_DRAW_HAND |
                                            SM.SM_DRAW_SECTORS |
                                            SM.SM_DRAW_MIDDLE_TEXT |
                                            SM.SM_DRAW_SECONDARY_TICKS
                                            )

        # Set The Region Of Existence Of SpeedMeter (Always In Radians!!!!)
        self.SpeedWindow1.SetAngleRange(-pi/6, 7*pi/6)

        # Create The Intervals That Will Divide Our SpeedMeter In Sectors        
        #intervals = range(0, 201, 20)
        intervals=self.intervals1
        self.SpeedWindow1.SetIntervals(intervals)

        # Assign The Same Colours To All Sectors (We Simulate A Car Control For Speed)
        # Usually This Is Black
        colours = [wx.BLACK]*10
        self.SpeedWindow1.SetIntervalColours(colours)

        # Assign The Ticks: Here They Are Simply The String Equivalent Of The Intervals

        ticks = [str(interval) for interval in intervals]
        self.SpeedWindow1.SetTicks(ticks)
        # Set The Ticks/Tick Markers Colour
        self.SpeedWindow1.SetTicksColour(wx.WHITE)
        # We Want To Draw 5 Secondary Ticks Between The Principal Ticks
        self.SpeedWindow1.SetNumberOfSecondaryTicks(5)

        # Set The Font For The Ticks Markers
        self.SpeedWindow1.SetTicksFont(wx.Font(13, wx.SWISS, wx.NORMAL, wx.NORMAL))
                                        
        # Set The Text In The Center Of SpeedMeter
        self.SpeedWindow1.SetMiddleText(self.SetMiddleText)

        # Assign The Colour To The Center Text
        self.SpeedWindow1.SetMiddleTextColour(wx.WHITE)
        # Assign A Font To The Center Text
        self.SpeedWindow1.SetMiddleTextFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD))

        # Set The Colour For The Hand Indicator
        self.SpeedWindow1.SetHandColour(wx.Colour(255, 50, 0))

        # Do Not Draw The External (Container) Arc. Drawing The External Arc May
        # Sometimes Create Uglier Controls. Try To Comment This Line And See It
        # For Yourself!
        self.SpeedWindow1.DrawExternalArc(False)        

        # Set The Current Value For The SpeedMeter
        self.SpeedWindow1.SetSpeedValue(6)


        self.SpeedWindow5 = SM.SpeedMeter(Dialpanel,
                                            extrastyle=SM.SM_DRAW_HAND |
                                            SM.SM_DRAW_PARTIAL_SECTORS |
                                            SM.SM_DRAW_SECONDARY_TICKS |
                                            SM.SM_DRAW_MIDDLE_TEXT |
                                            SM.SM_ROTATE_TEXT
                                            )

        # We Want To Simulate The Round Per Meter Control In A Car
        self.SpeedWindow5.SetAngleRange(-pi/6, 7*pi/6)

        intervals = range(0, 9)
        self.SpeedWindow5.SetIntervals(intervals)

        colours = [wx.BLACK]*6
        colours.append(wx.Colour(255, 255, 0))
        colours.append(wx.RED)
        self.SpeedWindow5.SetIntervalColours(colours)

        ticks = [str(interval) for interval in intervals]
        self.SpeedWindow5.SetTicks(ticks)
        self.SpeedWindow5.SetTicksColour(wx.WHITE)
        self.SpeedWindow5.SetTicksFont(wx.Font(13, wx.SWISS, wx.NORMAL, wx.NORMAL))

        self.SpeedWindow5.SetHandColour(wx.Colour(255, 50, 0))

        self.SpeedWindow5.SetSpeedBackground(wx.SystemSettings_GetColour(0))        

        self.SpeedWindow5.DrawExternalArc(False)

        self.SpeedWindow5.SetShadowColour(wx.Colour(50, 50, 50))

        self.SpeedWindow5.SetMiddleText("rpm")
        self.SpeedWindow5.SetMiddleTextColour(wx.WHITE)
        self.SpeedWindow5.SetMiddleTextFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.SpeedWindow5.SetSpeedBackground(wx.Colour(0, 0, 0)) 
        
        self.SpeedWindow5.SetSpeedValue(.2)
        

        ####################################################################
        # This little bit of magic keeps the list the same size as the frame
        def OnPSize(e, win = Dialpanel):
            Dialpanel.SetSize(e.GetSize())
            self.SpeedWindow1.SetSize(e.GetSize())
            self.SpeedWindow5.SetSize(e.GetSize())

            w,h = self.frame.GetClientSizeTuple()
            self.SpeedWindow1.SetDimensions(0,HOFFSET_LIST+12, w/2-7 ,h )
            self.SpeedWindow5.SetDimensions(w/2+2,HOFFSET_LIST+12, w/2-7 , h )


        Dialpanel.Bind(wx.EVT_SIZE,OnPSize)
        ####################################################################

        self.nb.AddPage(Dialpanel, "Gauges")
##########
    def build_dial_page1(self):

        HOFFSET_LIST=0 #offset from the top of panel (space for buttons)
        Dialpanel1 = wx.Panel(self.nb, -1)
        self.Dialpanel1=Dialpanel1
        tID = wx.NewId()
        self.dial_id = tID

        # Third SpeedMeter: We Use The Following Styles:
        #
        # SM_DRAW_HAND: We Want To Draw The Hand (Arrow) Indicator

        # SM_DRAW_PARTIAL_SECTORS: Partial Sectors Will Be Drawn, To Indicate Different Intervals
        # SM_DRAW_MIDDLE_ICON: We Draw An Icon In The Center Of SpeedMeter
        
        self.SpeedWindow3 = SM.SpeedMeter(Dialpanel1,
                                            extrastyle=SM.SM_DRAW_HAND |
                                            SM.SM_DRAW_PARTIAL_SECTORS |
                                            SM.SM_DRAW_MIDDLE_TEXT |
                                            SM.SM_DRAW_TITLE_TEXT |
                                            SM.SM_DRAW_SUB_TEXT |
                                            SM.SM_DRAW_SUB2_TEXT
                                            )
 
        # We Want To Simulate A Car Gas-Control
        self.SpeedWindow3.SetAngleRange(-pi/3, pi/3)

        intervals = range(0,5)
        self.SpeedWindow3.SetIntervals(intervals)

        colours = [wx.RED]*1
        colours.append('#FFFF00')
        colours.append(wx.BLACK)
        colours.append(wx.BLACK)

        self.SpeedWindow3.SetIntervalColours(colours)

        ticks = ["", "3", "2", "1", ""]
        self.SpeedWindow3.SetTicks(ticks)
        self.SpeedWindow3.SetTicksColour(wx.WHITE)
        self.SpeedWindow3.SetTicksFont(wx.Font(10, wx.SCRIPT, wx.NORMAL, wx.BOLD, True))
        self.SpeedWindow3.SetMiddleText("bar")
        self.SpeedWindow3.SetMiddleTextColour(wx.WHITE)
        self.SpeedWindow3.SetMiddleTextFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD))

        self.SpeedWindow3.SetHandColour(wx.Colour(255, 255, 0))
        if SX==0:
             meas="g/s"
        else:
             meas="lb/s"
        self.SpeedWindow3.SetTitleText("MAP\n(intake manifold absolute pressure)\n\n\n\nMAF (Air Flow Rate) "+meas) #
        self.SpeedWindow3.SetSubText("Max: -") #
        self.SpeedWindow3.SetSub2Text("Cur: -\nMax: -")

        # Draw The Icon In The Center Of SpeedMeter        
        #self.SpeedWindow3.SetMiddleIcon(icon)        

        self.SpeedWindow3.SetSpeedBackground(wx.BLACK)        

        self.SpeedWindow3.SetArcColour(wx.WHITE)
        
        self.SpeedWindow3.SetSpeedValue(0)

        

        ####################################################################
        # This little bit of magic keeps the list the same size as the frame
        def OnPSize(e, win = Dialpanel1):
            Dialpanel1.SetSize(e.GetSize())
            self.SpeedWindow3.SetSize(e.GetSize())

            w,h = self.frame.GetClientSizeTuple()
#            self.SpeedWindow3.SetDimensions(0,0, w/2.2 ,h-25 )
            self.SpeedWindow3.SetDimensions(0,0, w+200 ,h-25 )



        Dialpanel1.Bind(wx.EVT_SIZE,OnPSize)
        ####################################################################

        self.nb.AddPage(Dialpanel1, "Turbo")



#########
        
    def TraceDebug(self,level,msg):
        if self.DEBUGLEVEL<=level:
            self.trace.Append([str(level),msg])
        
    def OnInit(self):
        global SX, display

        display = os.popen('gconftool-2 -g /system/osso/dsm/display/display_dim_timeout').read()
        display=int(display)

        if display==2345:
              display=60

        os.popen('gconftool-2 -s /system/osso/dsm/display/display_dim_timeout -t int '+ str(2345))

        self.ThreadControl = 0 #say thread what to do
        self.COMPORT = 0
        self.senprod = None
        self.DEBUGLEVEL = 0 #debug everthing
        self.btname = ""

        tID = wx.NewId()

        #read settings from file
        self.config = ConfigParser.RawConfigParser()
                
        #print platform.system()
        #print platform.mac_ver()[]        
        
        if "OS" in os.environ.keys(): #runnig under windows
            self.configfilepath="pyobd.ini"
        else:
            self.configfilepath=os.environ['HOME']+'/.pyobdrc'
        #if self.config.read(self.configfilepath)==[]:

        self.COMPORT="/dev/ttyACM0"
        self.RECONNATTEMPTS=5
        self.SERTIMEOUT=2

        try:
            BToff = os.popen('cat /home/opt/pyobd/pyobd.conf | awk \'/BToff/ {print $2}\'').read()
        except:
            pass

        lungh = len(str(BToff).strip())

        if lungh==0 or lungh==(4):
            BToff=True
        else:
            BToff=False

        self.BToff=BToff

        try:
            gauges_mph = os.popen('cat /home/opt/pyobd/pyobd.conf | awk \'/gauges/ {print $2}\'').read()
        except:
            pass

        lungh = len(str(gauges_mph).strip())
        if lungh==(4):
            gauges_mph=True
            SX=1
        else:
            gauges_mph=False
            SX=0

        self.gauges_mph=gauges_mph

        #print self.gauges_mph
        if self.gauges_mph==True:
                self.k=0.625
                self.SetMiddleText="mph"
                self.intervals1 = range(0, 151, 15)
        else:
                self.k=1
                self.SetMiddleText="Km/h"
                self.intervals1 = range(0, 201, 20)

        #else:
        #    self.COMPORT=self.config.get("pyOBD","COMPORT")
        #    self.RECONNATTEMPTS=self.config.getint("pyOBD","RECONNATTEMPTS")
        #    self.SERTIMEOUT=self.config.getint("pyOBD","SERTIMEOUT")
        
        frame = wx.Frame(None, -1, "pyOBD-II for n900")
        frame.ShowFullScreen(True) 
        self.frame=frame

        EVT_RESULT(self,self.OnResult,EVT_RESULT_ID)
        EVT_RESULT(self,self.OnDebug, EVT_DEBUG_ID)
        EVT_RESULT(self,self.OnDtc,EVT_DTC_ID)
        EVT_RESULT(self,self.OnDial,EVT_DIAL_ID)
        EVT_RESULT(self,self.OnDial1,EVT_DIAL1_ID)
        EVT_RESULT(self,self.OnStatus,EVT_STATUS_ID)
        EVT_RESULT(self,self.OnTests,EVT_TESTS_ID)
        
        # Main notebook frames
        self.nb = wx.Notebook(frame, -1, style = wx.NB_TOP)
        self.nb.SetFont(wx.FFont(16, wx.DEFAULT)) 

        self.status = self.MyListCtrl(self.nb, tID,style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.status.InsertColumn(0, "Description",width=280)
        self.status.InsertColumn(1, "Value")
        self.status.Append(["Link State","Disconnnected"]);
        self.status.Append(["Protocol","---"]);
        self.status.Append(["Cable version","---"]);
        self.status.Append(["COM port",self.COMPORT]);

        self.status.Append(["BT dev",self.btname]);
        
        self.nb.AddPage(self.status, "Status")
        
        self.OBDTests = self.MyListCtrl(self.nb, tID,style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.OBDTests.InsertColumn(0, "Description",width=280)
        self.OBDTests.InsertColumn(1, "Value")
        self.nb.AddPage(self.OBDTests, "Tests")
                
        for i in range(0,len(ptest)): #fill MODE 1 PID 1 test description 
            self.OBDTests.Append([ptest[i],"---"]);
        
        self.build_sensor_page()

        self.build_DTC_page()
        self.build_dial_page()
        self.build_dial_page1()
        
        self.trace = self.MyListCtrl(self.nb, tID,style=wx.LC_REPORT|wx.SUNKEN_BORDER)
        self.trace.InsertColumn(0, "Level",width=70)
        self.trace.InsertColumn(1, "Message")
        self.nb.AddPage(self.trace, "Trace")
        self.TraceDebug(1,"Application started")

        # Setting up the menu.
        self.filemenu= wx.Menu()
        self.filemenu.Append(ID_FULLSCREEN,"FullScreen ON/OFF","FullScreen")
        self.filemenu.AppendSeparator()
        self.filemenu.Append(ID_CSVLOG,"Sensor CSV logging ON/OFF","CSVLOG")
        self.filemenu.AppendSeparator()
        self.filemenu.Append(ID_GAUGES,"Select Units of Measurements","GAUGES")
        self.filemenu.AppendSeparator()
        self.filemenu.Append(ID_EXIT,"Exit"," Terminate the program")

        self.settingmenu = wx.Menu()
        self.settingmenu.Append(ID_CONFIG,"Search and connect paired BT dongle"," Configure pyOBD")
        self.settingmenu.AppendSeparator()
        self.settingmenu.Append(ID_RESET,"Connect to ECU"," Reopen and connect to device")
        self.settingmenu.AppendSeparator()
        self.settingmenu.Append(ID_DISCONNECT,"Disconnect","Close connection to device")

        self.dtcmenu= wx.Menu()
        # tady toto nastavi automaticky tab DTC a provede akci
        self.dtcmenu.Append(ID_GETC    ,"Get DTCs",    " Get DTC Codes")
        self.dtcmenu.AppendSeparator()
        self.dtcmenu.Append(ID_CLEAR ,"Clear DTC",    " Clear DTC Codes")

#m        self.dtcmenu.Append(ID_LOOK    ,"Code Lookup"," Lookup DTC Codes")

        self.helpmenu = wx.Menu()
        self.helpmenu.Append(ID_HELP_ABOUT        ,"About this program",    " Get DTC Codes")
        self.helpmenu.AppendSeparator()
        self.helpmenu.Append(ID_HELP_ABOUT_Maemo    ,"About Maemo/n900",    " ")
        self.helpmenu.AppendSeparator()
        self.helpmenu.Append(ID_HELP_VISIT_Maemo    ,"Visit MAEMO Thread"," ")
        self.helpmenu.AppendSeparator()
        self.helpmenu.Append(ID_HELP_VISIT        ,"Visit program homepage"," Lookup DTC Codes")
        #self.helpmenu.Append(ID_HELP_ORDER        ,"Order OBD-II cables",    " Clear DTC Codes")


        # Creating the menubar.
        self.menuBar = wx.MenuBar()
        self.menuBar.Append(self.filemenu,"\n File ") # Adding the "filemenu" to the MenuBar
        self.menuBar.Append(self.settingmenu,"\n OBD-II ")
        self.menuBar.Append(self.dtcmenu,"\n Trouble codes ")
        self.menuBar.Append(self.helpmenu,"\n Help")
        
        frame.SetMenuBar(self.menuBar)    # Adding the MenuBar to the Frame content.

        frame.Bind(wx.EVT_MENU,self.On_gauge_mph_kmh,id=ID_GAUGES)
        frame.Bind(wx.EVT_MENU,self.OnFullScreen,id=ID_FULLSCREEN)
        frame.Bind(wx.EVT_MENU,self.OnCSVLogOnOff,id=ID_CSVLOG)
        frame.Bind(wx.EVT_MENU,self.OnExit,id=ID_EXIT) # attach the menu-event ID_EXIT to the    
        frame.Bind(wx.EVT_MENU,self.QueryClear,id=ID_CLEAR)
        frame.Bind(wx.EVT_MENU,self.Configure,id=ID_CONFIG)
        frame.Bind(wx.EVT_MENU,self.OpenPort,id=ID_RESET)
        frame.Bind(wx.EVT_MENU,self.OnDisconnect,id=ID_DISCONNECT)
        frame.Bind(wx.EVT_MENU,self.GetDTC,id=ID_GETC)
        frame.Bind(wx.EVT_MENU,self.CodeLookup,id=ID_LOOK)
        frame.Bind(wx.EVT_MENU,self.OnHelpAbout,id=ID_HELP_ABOUT) 

        frame.Bind(wx.EVT_MENU,self.OnHelpAboutMaemo,id=ID_HELP_ABOUT_Maemo) 
        frame.Bind(wx.EVT_MENU,self.OnVisitMaemo,id=ID_HELP_VISIT_Maemo) 

        frame.Bind(wx.EVT_MENU,self.OnHelpVisit,id=ID_HELP_VISIT)
        frame.Bind(wx.EVT_MENU,self.OnHelpOrder,id=ID_HELP_ORDER)
    
        self.SetTopWindow(frame)

        frame.Show(True)
        frame.SetSize((800,400))
        self.sensor_control_off()
        self.settingmenu.Enable(ID_RESET,False)
        self.settingmenu.Enable(ID_DISCONNECT,False)

        return True


    def OnVisitMaemo(self,event):
        stringa = 'dbus-send --system --type=method_call --dest=com.nokia.osso_browser /com/nokia/osso_browser/request com.nokia.osso_browser.load_url string:"http://talk.maemo.org/showthread.php?t=81129"'
        os.popen(stringa)


    def OnHelpVisit(self,event):
        #webbrowser.open("http://www.obdtester.com/pyobd")
        stringa = 'dbus-send --system --type=method_call --dest=com.nokia.osso_browser /com/nokia/osso_browser/request com.nokia.osso_browser.load_url string:"http://www.obdtester.com/pyobd"'
        os.popen(stringa)


    def OnHelpOrder(self,event):
        #webbrowser.open("http://www.obdtester.com/order")
        stringa = 'dbus-send --system --type=method_call --dest=com.nokia.osso_browser /com/nokia/osso_browser/request com.nokia.osso_browser.load_url string:"http://www.obdtester.com/order"'
        os.popen(stringa)


    def OnHelpAboutMaemo(self,event):
        stringa = 'dbus-send --print-reply --dest=com.nokia.osso_notes /com/nokia/osso_notes com.nokia.osso_notes.mime_open string:/home/opt/pyobd/pyobd.txt'
        os.popen(stringa)

        Text = """ pyobd

Adapted to n900 by m750 at maemo.org
"""

        #self.HelpAboutMaemoDlg = wx.MessageDialog(self.frame, Text, 'About n900 adapting',wx.OK | wx.ICON_INFORMATION)
        #self.HelpAboutMaemoDlg.ShowModal()
        #self.HelpAboutMaemoDlg.Destroy()

    
    def OnHelpAbout(self,event): #todo about box
        Text = """PyOBD is an automotive OBD2 diagnosting app. using ELM237 cable.
(C) 2008-2009 SeCons Ltd.
(C) 2004 Charles Donour Sizemore
www.obdtester.com    www.secons.com
PyOBD is free software; you can 
redistribute it and/or modify it under the 
terms of the GNU General Public License 
as published by the Free SW Foundation; 
either version 2 of the License, or any 
later version.    

"""


#    PyOBD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MEHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with PyOBD; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA

        #HelpAboutDlg = wx.Dialog(self.frame, 1, title="About")
        
        #box    = wx.BoxSizer(wx.HORIZONTAL)
        #box.Add(wx.StaticText(reconnectPanel,-1,Text,pos=(0,0),size=(200,200)))
        #box.Add(wx.Button(HelpAboutDlg,wx.ID_OK),0)
        #box.Add(wx.Button(HelpAboutDlg,wx.ID_CANCEL),1)

        #HelpAboutDlg.SetSizer(box)
        #HelpAboutDlg.SetAutoLayout(True)
        #sizer.Fit(HelpAboutDlg)
        #HelpAboutDlg.ShowModal()
        
        self.HelpAboutDlg = wx.MessageDialog(self.frame, Text, 'About PyOBD' ,wx.OK | wx.ICON_INFORMATION)
        self.HelpAboutDlg.ShowModal()
        self.HelpAboutDlg.Destroy()
        
    def OnResult(self,event):
        self.sensors.SetStringItem(event.data[0], event.data[1], event.data[2])
    

    def OnStatus(self,event):
        if event.data[0] == 666: #signal, that connection falied
            self.sensor_control_off()
        else:
            self.status.SetStringItem(event.data[0], event.data[1], event.data[2])
    

    def OnTests(self,event):
        self.OBDTests.SetStringItem(event.data[0], event.data[1], event.data[2])
         

    def OnDebug(self,event):
        self.TraceDebug(event.data[0],event.data[1])
    

    def OnDtc(self,event):
        if event.data == 0: #signal, that DTC was cleared
            self.dtc.DeleteAllItems()
        else:
            self.dtc.Append(event.data)


    def OnDial(self,event):
        wx.Yield()
        data=event.data
        print data
        d1=data[:3]
        d2=int(data[3:])
        if d1=="spe":
    
            if d2>200 and self.k==1:
                d2=200
                self.SpeedWindow1.SetTicksColour(wx.RED)
            elif d2>240 and self.k==0.625:
                d2=240
                self.SpeedWindow1.SetTicksColour(wx.RED)
    
            else:
                self.SpeedWindow1.SetTicksColour(wx.WHITE)
    
            self.SpeedWindow1.SetSpeedValue(d2)
    
        elif d1=="rpm":
            #print "rpm: "+str(d2)
            if d2>8000:
                d2=8000
                self.SpeedWindow5.SetTicksColour(wx.RED)
            else:
                self.SpeedWindow5.SetTicksColour(wx.WHITE)
            d2=d2/1000.0
    
            self.SpeedWindow5.SetSpeedValue(d2)
    
    def OnDial1(self,event):
        global maxbar, maxmaf
        wx.Yield()
        data=event.data
    
        d1=data[:3]
        d2=data[3:]
    
        if d1=="MAP":
            d2=int(d2)/100.0
            if d2>maxbar:
                maxbar=d2
                self.SpeedWindow3.SetSubText("Max: "+str(d2))
            wx.Yield()
            if d2>4:
                d2=4
            d2=4-d2
            self.SpeedWindow3.SetSpeedValue(d2)
    
        elif d1=="MAF":
            d2=float(d2)
            if d2>maxmaf:
                maxmaf=d2
            d2=str(d2)                                 # +"#"+str(d2*0.0022)[:6]
            text="Cur: "+str(d2)+"\nMax: "+str(maxmaf) # +"#"+str(maxmaf*0.0022)[:6]
            self.SpeedWindow3.SetSub2Text(text)


    def OnDisconnect(self,event): #disconnect connection to ECU
        global btaddr, seriale, node, manager, bus, adapter, target_address , service, path
        print "stop"

        #w = sensorProduce()
        #w.ws.PostEvent(self._notify_window, StatusEvent([0,1,"Disconnected"]))

        time.sleep(0.1)
        self.btname = " "
        self.ThreadControl=666
        self.sensor_control_off()

        bus = dbus.SystemBus()
        manager = dbus.Interface(bus.get_object("org.bluez", "/"),"org.bluez.Manager")
        adapter = dbus.Interface(bus.get_object("org.bluez", manager.DefaultAdapter()),"org.bluez.Adapter")

        #adapter.SetProperty('Powered',False)
        node = seriale.Disconnect(service)

        time.sleep(0.1)
        self.settingmenu.Enable(ID_RESET,False)

        
    def OpenPort(self,e):
        if self.senprod: # signal current producers to finish
            self.senprod.stop()
        self.ThreadControl = 0    
        self.senprod = self.sensorProducer(self,self.COMPORT,self.SERTIMEOUT,self.RECONNATTEMPTS,self.nb)
        self.senprod.start() 
        
        self.sensor_control_on()
        

    def GetDTC(self,e):
        self.nb.SetSelection(3)
        self.ThreadControl=2
        

    def AddDTC(self, code):
        self.dtc.InsertStringItem(0, "")
        self.dtc.SetStringItem(0, 0, code[0])
        self.dtc.SetStringItem(0, 1, code[1])


    def CodeLookup(self,e = None):
        id = 0
        diag = wx.Frame(None, id, title="Diagnostic Trouble Codes")

        tree = wx.TreeCtrl(diag, id, style = wx.TR_HAS_BUTTONS)

        root = tree.AddRoot("Code Reference")
        proot = tree.AppendItem(root,"Powertrain (P) Codes")
        codes = obd_io.pcodes.keys()


        codes.sort()
        group = ""
        for c in codes:
            if c[:3] != group:
                group_root = tree.AppendItem(proot, c[:3]+"XX")
                group = c[:3]
            leaf = tree.AppendItem(group_root, c)
            tree.AppendItem(leaf, obd_io.pcodes[c]) 

        uroot = tree.AppendItem(root,"Network (U) Codes")
        codes = obd_io.ucodes.keys()
        codes.sort()
        group = ""
        for c in codes:
            if c[:3] != group:
                group_root = tree.AppendItem(uroot, c[:3]+"XX")
                group = c[:3]
            leaf = tree.AppendItem(group_root, c)
            tree.AppendItem(leaf, obd_io.ucodes[c]) 

        diag.SetSize((400,500))
        diag.Show(True)

        
    def QueryClear(self,e):
        id = 0
        diag = wx.Dialog(self.frame, id, title="Clear DTC?")

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(wx.StaticText(diag, -1, "Are you sure you wish to"),0)
        sizer.Add(wx.StaticText(diag, -1, "clear all DTC codes and "),0)
        sizer.Add(wx.StaticText(diag, -1, "freeze frame data?        "),0)
        box    = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(wx.Button(diag,wx.ID_OK,     "Ok"    ),0)
        box.Add(wx.Button(diag,wx.ID_CANCEL, "Cancel"),0)

        sizer.Add(box, 0)
        diag.SetSizer(sizer)
        diag.SetAutoLayout(True)
        sizer.Fit(diag)
        r    = diag.ShowModal()
        if r == wx.ID_OK:
            self.ClearDTC()

    def ClearDTC(self):
        self.ThreadControl=1
        self.nb.SetSelection(3)
        
    
    def scanSerial(self):
        global btaddr, seriale, node, manager, bus, adapter, target_address , service, path

#rfcomm bluetooth
        global found, btname, btaddr
        target_address = None
        number_of_tries = 2
        btname=""

# nome


            # Loop a couple times in case the device/name isn't found the first time
        while target_address is None and number_of_tries > 0:
             number_of_tries = number_of_tries - 1

             # Discover bluetooth devices in the vicinity
             print "Searching for BT dev..."
             nearby_devices = discover_devices()

             # Loop through each device and check its name for a prefix match
             found=0
             for btaddr in nearby_devices:
                btname = lookup_name(btaddr)
                print " -Found device: %s (%s)" % (btname, btaddr)

                try:
                    if len(btname) >= 2:
                        found=1
                        # Use the first device it finds that matches name
                        print ' Found BT SPP dev'
                        os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"Found a BT dev. Please wait"')
    
                        target_address = btaddr
    
                        bus = dbus.SystemBus()
                        manager = dbus.Interface(bus.get_object("org.bluez", "/"),"org.bluez.Manager")
                        adapter = dbus.Interface(bus.get_object("org.bluez", manager.DefaultAdapter()),"org.bluez.Adapter")
                        address = btaddr
                        service = "spp"
                        path = adapter.FindDevice(address)
                        seriale = dbus.Interface(bus.get_object("org.bluez", path),"org.bluez.Serial")
                        try:
                            node = seriale.Disconnect(service)
                        except:
                            pass

                        try:
                            node = seriale.Connect(service)
                        except:
                            print "Can't open BT serial port"
                            os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"Can\'t open BT serial port"')
                            found=2

                 #print "Connected %s to %s" % (node, address)
                 #print "Press CTRL-C to disconnect"
                 #a = open("/dev/rfcomm0","rw")
                    else:
                        print "NOT FOUND"
                except:
                    if found==0:
                        os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"BT Error, please retry"')

             break

#rfcomm bluetooth

        """scan for available ports. return a list of serial names"""
        available = []

#
#        for i in range(256):
#            try: #scan standart ttyS*
#                s = serial.Serial(i)
#                available.append(s.portstr)
#                s.close()    # explicit close 'cause of delayed GC in java
#            except serial.SerialException:
#                pass
#        for i in range(256):
#            try: #scan USB ttyACM
#                s = serial.Serial("/dev/ttyACM"+str(i))
#                available.append(s.portstr)
#                s.close()    # explicit close 'cause of delayed GC in java
#            except serial.SerialException:
#                pass
#        for i in range(256):
#            try:
#                s = serial.Serial("/dev/ttyUSB"+str(i))
#                available.append( (i, s.portstr))
#                s.close()    # explicit close 'cause of delayed GC in java
#            except serial.SerialException:
#                pass
#        for i in range(256):
#            try:
#                s = serial.Serial("/dev/ttyd"+str(i))
#                available.append( (i, s.portstr))
#                s.close()    # explicit close 'cause of delayed GC in java
#            except serial.SerialException:
#                pass


        for i in range(10):
            try:
                s = serial.Serial("/dev/rfcomm"+str(i))
                #available.append( (i, s.portstr))
                available.append( (i, str(s.port)))
                s.close()    # explicit close 'cause of delayed GC in java
            except serial.SerialException:
                pass

            
        # ELM-USB shows up as /dev/tty.usbmodemXXXX, where XXXX is a changing hex string
        # on connection; so we have to search through all 64K options
        if len(platform.mac_ver()[0])!=0:    #search only on MAC
            for i in range (65535):
                extension = hex(i).replace("0x","", 1)
                try:
                    s = serial.Serial("/dev/tty.usbmodem"+extension)
                    available.append(s.portstr)
                    s.close()
                except serial.SerialException:
                    pass 
        
        return available

    def Configure(self,e = None):
        global found, btname
        id = 0
        self.btname = " "
        bus = dbus.SystemBus()
        manager = dbus.Interface(bus.get_object("org.bluez", "/"),"org.bluez.Manager")
        adapter = dbus.Interface(bus.get_object("org.bluez", manager.DefaultAdapter()),"org.bluez.Adapter")
        adapter.SetProperty('Powered',True)
        time.sleep(0.6)

        os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"Searching for paired BT dev. Please wait..."')

        self.status.SetStringItem(3,1,""); 
        self.status.SetStringItem(4,1,""); 
        diag = wx.Dialog(self.frame, id, title="Configure")
        sizer = wx.BoxSizer(wx.VERTICAL)
        
        c1 = self.scanSerial()
        c2=str(c1)
        c3=c2[6:18]
        ports=c3
        print "Port: "+ports
        self.portName = ports
        self.btname = btname

#        rb = wx.RadioBox(diag, id, "Choose Serial Port", choices = ports, style = wx.RA_SPECIFY_COLS, majorDimension = 20)

                
#        sizer.Add(rb, 0)

        #timeOut input control                
        timeoutPanel = wx.Panel(diag, -1)
        timeoutCtrl = wx.TextCtrl(timeoutPanel, -1, '',pos=(140,0), size=(35, 30))
        timeoutStatic = wx.StaticText(timeoutPanel,-1,'Timeout:',pos=(3,5),size=(140,30))
        timeoutCtrl.SetValue(str(self.SERTIMEOUT))
        
        #reconnect attempt input control                
        reconnectPanel = wx.Panel(diag, -1)
        reconnectCtrl = wx.TextCtrl(reconnectPanel, -1, '',pos=(140,0), size=(35, 30))
        reconnectStatic = wx.StaticText(reconnectPanel,-1,'Reconnect attempts:',pos=(3,5),size=(140,30))
        reconnectCtrl.SetValue(str(self.RECONNATTEMPTS))

        #automatic switch off of the BT
        BT_Panel = wx.Panel(diag, -1)
        BT_Static = wx.CheckBox(BT_Panel,-1,'BT OFF on EXIT',pos=(3,5),size=(250,30))
        BT_Static.SetValue(self.BToff)


        
        #web open link button
#m        self.OpenLinkButton = wx.Button(diag,-1,"Click here to order ELM-USB interface",size=(260,30))
#m        diag.Bind(wx.EVT_BUTTON,self.OnHelpOrder,self.OpenLinkButton)
        
        #set actual serial port choice
#m        if (self.COMPORT != 0) and (self.COMPORT in ports):
#m            rb.SetSelection(ports.index(self.COMPORT))

        
#m        sizer.Add(self.OpenLinkButton)
        if found==1:
            stringa="Found: "+btname+" - "+ports + " - Try to connect?"
        else:
            stringa="BlueTooth OBD device NOT detected"

        sizer.Add(wx.StaticText(diag, id, stringa))

        sizer.Add(timeoutPanel,0)
        sizer.Add(reconnectPanel,0)

        sizer.Add(BT_Panel,0)


        box    = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(wx.Button(diag,wx.ID_OK),0)
        box.Add(wx.Button(diag,wx.ID_CANCEL),1)

        sizer.Add(box, 0)
        diag.SetSizer(sizer)
        diag.SetAutoLayout(True)
        sizer.Fit(diag)
        if found==1:
            r = diag.ShowModal()

            if r == wx.ID_OK:
                os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"OK, now select \'OBD-II\', then \'Connect to ECU\'"')
                self.settingmenu.Enable(ID_RESET,True)
                time.sleep(0.1)
                self.BToff=BT_Static.GetValue()

                if self.BToff==True:
                    log2='echo "BToff True" > /home/opt/pyobd/pyobd.conf'
                    os.popen(log2)
                else:
                    log2='echo "BToff False" > /home/opt/pyobd/pyobd.conf'
                    os.popen(log2)

                if self.gauges_mph==True:
                    log2='echo "gauges True" >> /home/opt/pyobd/pyobd.conf'
                    os.popen(log2)
                else:
                    log2='echo "gauges False" >> /home/opt/pyobd/pyobd.conf'
                    os.popen(log2)

            
            #create section
                if self.config.sections()==[]:
                     self.config.add_section("pyOBD")
            #set and save COMPORT
#m                    self.COMPORT = ports[rb.GetSelection()]
                     self.COMPORT = ports
                     self.config.set("pyOBD","COMPORT",self.COMPORT) 
            
            #set and save SERTIMEOUT
                     self.SERTIMEOUT = int(timeoutCtrl.GetValue())
                     self.config.set("pyOBD","SERTIMEOUT",self.SERTIMEOUT)
                     self.status.SetStringItem(3,1,self.COMPORT); 
                     self.status.SetStringItem(4,1,self.btname); 
            
            #set and save RECONNATTEMPTS
                     self.RECONNATTEMPTS = int(reconnectCtrl.GetValue())
                     self.config.set("pyOBD","RECONNATTEMPTS",self.RECONNATTEMPTS)
            
            #write configuration to cfg file
                     self.config.write(open(self.configfilepath, 'wb'))

                     self.settingmenu.Enable(ID_RESET,True)
            #self.settingmenu.Enable(ID_DISCONNECT,True)

        elif found==2:
            time.sleep(0.7)
            os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"BT dev. Found, but can\'t open serial port"')
            time.sleep(0.7)
            os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"BT dev. Found, but can\'t open serial port"')

        else:
            print "BlueTooth OBD device NOT detected"
            time.sleep(0.7)
            os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"No BT dev. Found"')
            time.sleep(0.7)


    def OnFullScreen(self,e = None):
        self.frame.ShowFullScreen(not self.frame.IsFullScreen(), wx.FULLSCREEN_ALL)

    def OnCSVLogOnOff(self,e = None):
        global CSVLogEnabled
        if CSVLogEnabled == 0:
            CSVLogEnabled = 1
            os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"Logging sensor values into CSV ENABLED"')
        else:
            CSVLogEnabled = 0
            os.popen('dbus-send --type=method_call --dest=org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications.SystemNoteInfoprint string:"Logging sensor values into CSV DISABLED"')

    def On_gauge_mph_kmh(self,e = None):
        id=0
        diag = wx.Dialog(self.frame, id, title="Select Units of Measurements")
        sizer = wx.BoxSizer(wx.VERTICAL)

        mph_Panel = wx.Panel(diag, -1)
        mph_Static = wx.CheckBox(mph_Panel,-1,' US/Imperial instead Metrical units\n    (To apply the changes, close and reopen pyOBD)',pos=(3,5),size=(700,100))
        mph_Static.SetValue(self.gauges_mph)

        sizer.Add(mph_Panel,0)

        box    = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(wx.Button(diag,wx.ID_OK),0)
        #box.Add(wx.Button(diag,wx.ID_CANCEL),1)

        sizer.Add(box, 0)
        diag.SetSizer(sizer)
        diag.SetAutoLayout(True)
        sizer.Fit(diag)
        r = diag.ShowModal()

        if r == wx.ID_OK:
                time.sleep(0.1)
                self.gauges_mph=mph_Static.GetValue()

                if self.BToff==True:
                    log2='echo "BToff True" > /home/opt/pyobd/pyobd.conf'
                    os.popen(log2)
                else:
                    log2='echo "BToff False" > /home/opt/pyobd/pyobd.conf'
                    os.popen(log2)

                if self.gauges_mph==True:
                    log2='echo "gauges True" >> /home/opt/pyobd/pyobd.conf'
                    os.popen(log2)
                else:
                    log2='echo "gauges False" >> /home/opt/pyobd/pyobd.conf'
                    os.popen(log2)

        
    def OnExit(self,e = None):
        global display
        import sys

        os.popen('gconftool-2 -s /system/osso/dsm/display/display_dim_timeout -t int '+ str(display))
        display = os.popen('gconftool-2 -g /system/osso/dsm/display/display_dim_timeout').read()

        if self.BToff==True:
            bus = dbus.SystemBus()
            manager = dbus.Interface(bus.get_object("org.bluez", "/"),"org.bluez.Manager")
            adapter = dbus.Interface(bus.get_object("org.bluez", manager.DefaultAdapter()),"org.bluez.Adapter")
            adapter.SetProperty('Powered',False)

        sys.exit(0)

app = MyApp(0)
app.MainLoop()
