 #!/usr/bin/env python
###########################################################################
# obd_sensors.py
#
# Copyright 2004 Donour Sizemore (donour@uchicago.edu)
# Copyright 2009 Secons Ltd. (www.obdtester.com)
#
# This file is part of pyOBD.
#
# pyOBD is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyOBD is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyOBD; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
###########################################################################
#mod by m750
###########################################################################

import decimal
decimal.getcontext().prec=5

#    numA = hex_to_int(code[:2]) #A byte
#    numB = hex_to_int(code[2:4]) #B byte
#    numC = hex_to_int(code[4:6]) #C byte
#    numD = hex_to_int(code[6:8]) #D byte

def hex_to_int(str):
    i = eval("0x" + str, {}, {})
    return i

def maf(code):
    #a = hex_to_int(code)
    #print a

    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    code=(numA*256+numB)/100.0
    lb=code * 0.002205
    return [code, lb]


def throttle_pos(code):
    code = hex_to_int(code)
    code = code * 100.0 / 255.0
    return [code, code]

def intake_m_pres(code): # in kPa
    code = hex_to_int(code)
    code = code / 0.14504
    return [code, code]
    
def rpm(code):
    code = hex_to_int(code)
    code = code / 4
    return [code, code]

def speed(code):
    code = hex_to_int(code)
    return [code,int(code/1.609)]

def percent_scale(code):
    code = hex_to_int(code)
    code = code * 100.0 / 255.0
    return [code, code]

def timing_advance(code):
    code = hex_to_int(code)
    code = (code - 128) / 2.0
    return [code, code]

def sec_to_min(code):
    code = hex_to_int(code)
    code = code / 60
    return [code, code]

def temp(code):
    code = hex_to_int(code)
    return [(code-40),(code-40)*9.0/5+32]

def cpass(code):
    #fixme
    code = hex_to_int(code)
    #fixme
    return [code, code]

def fuel_trim_percent(code):
    code = hex_to_int(code)
    code = (code - 128.0) * 100.0 / 128
    return [code, code]

def o2_sensor(code):
    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    return [numA/20.0, (numB-128)*100/128]


#m agg
def FuelRailPress(code):
    code = hex_to_int(code)
    kpa=(code) * 0.079
    psi=kpa*0.14504
    psi=decimal.Decimal(str(psi))
    psi=psi*1
    return [kpa, psi]

def FuelRailPressD(code):
    code = hex_to_int(code)
    kpa=(code) * 10
    psi=kpa*0.14504
    psi=decimal.Decimal(str(psi))
    psi=psi*1
    return [kpa, psi]

def per100(code):
    code = hex_to_int(code)
    code = (code * 100)
    return [code, code]

def per100_128(code):
    code = hex_to_int(code)
    code = (code * 100 / 128)
    return [code, code]

def o2sv(code):
    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    numC = hex_to_int(code[4:6]) #C byte
    numD = hex_to_int(code[6:8]) #D byte
    code = str( (numA*256+numB)*2/65535)+" "+str((numC*256+numD)*8/65535)
    return [code, code]

def o2sa(code):
    numA = hex_to_int(code[:2])  #A byte
    numB = hex_to_int(code[2:4]) #B byte
    numC = hex_to_int(code[4:6]) #C byte
    numD = hex_to_int(code[6:8]) #D byte

    code = str( (numA*256+numB)/32768)+" "+str((numC*256+numD)*256 - 128)
    return [code, code]

def max(code):
    numA = hex_to_int(code[:2])  #A byte
    numB = hex_to_int(code[2:4]) #B byte
    numC = hex_to_int(code[4:6]) #C byte
    numD = hex_to_int(code[6:8]) #D byte
    code = str(numA)+" "+str(numB)+" "+str(numC)+" "+str(numD*10)
    return [code, code]

def cmv(code):
    code = hex_to_int(code)
    code = (code/1000)
    return [code, code]

def catT(code):
    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    t=(numA*256+numB)/10 - 40
    return [t, int((t-40)*9.0/5+32)]

def oxsens2(code):
    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    code = str((numA-128)*100/128)+" "+str((numB-128)*100/128)
    return [code, code]

def abl(code):
    code = hex_to_int(code)
    code = (code*100/256)
    return [code, code]

def cer(code):
    code = hex_to_int(code)
    code = (code/32768)
    return [code, code]

def pid150(code):
    numA = hex_to_int(code[:2]) #A byte
    code = (numA*10)
    return [code, code]

def pid154(code):
    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    pa=(numA*256+numB-32768)
    psi=pa*0.0001450
    psi=decimal.Decimal(str(psi))
    psi=psi*1
    return [pa, psi]

def pid15d(code):
    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    code = (numA*256+numB-26880)/128
    return [code, code]

def pid15e(code):
    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    l = (numA*256+numB)*0.05
    g = l*0.264173
    return [l,g]

def pid010a(code):
    numA = hex_to_int(code[:2]) #A byte
    kpa=(numA*3)
    psi=kpa*0.14504
    psi=decimal.Decimal(str(psi))
    psi=psi*1
    return [kpa,psi]

def pid010b(code):
    numA = hex_to_int(code[:2]) #A byte
    kpa=numA
    psi=kpa*0.14504
    psi=decimal.Decimal(str(psi))
    psi=psi*1
    return [kpa,psi]

def pid0132(code):
    numA = hex_to_int(code[:2]) #A byte
    numB = hex_to_int(code[2:4]) #B byte
    pa=((numA*256)+numB)/4.0
    psi=pa*0.0001450
    psi=decimal.Decimal(str(psi))
    psi=psi*1
    return [pa,psi]


def pid151(code):
    fueltype=""
    code = hex_to_int(code)
    if code==1:
       fueltype="Gasoline"
    elif code==2:
       fueltype="Methanol"
    elif code==3:
       fueltype="Ethanol"
    elif code==4:
       fueltype="Diesel"
    elif code==5:
       fueltype="LPG"
    elif code==6:
       fueltype="CNG"
    elif code==7:
       fueltype="Propane"
    elif code==8:
       fueltype="Electric"
    elif code==9:
       fueltype="Bifuel running Gasoline"
    elif code==10:
       fueltype="Bifuel running Methanol"
    elif code==11:
       fueltype="Bifuel running Ethanol"
    elif code==12:
       fueltype="Bifuel running LPG"
    elif code==13:
       fueltype="Bifuel running CNG"
    elif code==14:
       fueltype="Bifuel running Prop"
    elif code==15:
       fueltype="Bifuel running Electricity"
    elif code==16:
       fueltype="Bifuel mixed gas/electric"
    elif code==17:
       fueltype="Hybrid gasoline"
    elif code==18:
       fueltype="Hybrid Ethanol"
    elif code==19:
       fueltype="Hybrid Diesel"
    elif code==20:
       fueltype="Hybrid Electric"
    elif code==21:
       fueltype="Hybrid Mixed fuel"
    elif code==22:
       fueltype="Hybrid Regenerative"
    else:
        fueltype="?"


    return [fueltype,(fueltype)]




def dtc_decrypt(code):
    #first byte is byte after PID and without spaces
    num = hex_to_int(code[:2]) #A byte
    res = []

    if num & 0x80: # is mil light on
        mil = 1
    else:
        mil = 0
        
    # bit 0-6 are the number of dtc's. 
    num = num & 0x7f
    
    res.append(num)
    res.append(mil)
    
    numB = hex_to_int(code[2:4]) #B byte
      
    for i in range(0,3):
        res.append(((numB>>i)&0x01)+((numB>>(3+i))&0x02))
    
    numC = hex_to_int(code[4:6]) #C byte
    numD = hex_to_int(code[6:8]) #D byte
       
    for i in range(0,7):
        res.append(((numC>>i)&0x01)+(((numD>>i)&0x01)<<1))
    
    res.append(((numD>>7)&0x01)) #EGR SystemC7  bit of different 
    
    return res

def hex_to_bitstring(str):
    #print "obd_sensors.hex_to_bistring(str): " + str
    bitstring = ""
    for i in str:
        # silly type safety, we don't want to eval random stuff
        if type(i) == type(''): 
            v = eval("0x%s" % i)
            if v & 8 :
                bitstring += '1'
            else:
                bitstring += '0'
            if v & 4:
                bitstring += '1'
            else:
                bitstring += '0'
            if v & 2:
                bitstring += '1'
            else:
                bitstring += '0'
            if v & 1:
                bitstring += '1'
            else:
                bitstring += '0'                
    return bitstring


class Sensor:
    def __init__(self,sensorName, sensorcommand, sensorValueFunction, u1,u2):
        self.name = sensorName
        self.cmd  = sensorcommand
        self.value= sensorValueFunction
        self.unit1 = u1
        self.unit2 = u2

SENSORS = [
    Sensor("Supported PIDs 1"             , "0100", hex_to_bitstring  ,"",""              ),    
    Sensor("Status Since DTC Cleared"     , "0101", dtc_decrypt       ,"",""              ),    
    Sensor("DTC Causing Freeze Frame"     , "0102", cpass             ,"BitEnc.","BitEnc."),    
    Sensor("Fuel System Status"           , "0103", cpass             ,"BitEnc.","BitEnc."),
    Sensor("Calculated Load Value"        , "0104", percent_scale     ,"",""              ),    
    Sensor("Coolant Temperature"          , "0105", temp              ,"deg. C","deg. F"  ),
    Sensor("Short Term Fuel % Trim B1"    , "0106", fuel_trim_percent ,"%","%"            ),
    Sensor("Long Term Fuel % Trim B1"     , "0107", fuel_trim_percent ,"%","%"            ),
    Sensor("Short Term Fuel % Trim B2"    , "0108", fuel_trim_percent ,"%","%"            ),
    Sensor("Long Term Fuel % Trim B2"     , "0109", fuel_trim_percent ,"%","%"            ),
    Sensor("Fuel Rail Pressure"           , "010A", pid010a           ,"kPa","psi"        ),
    Sensor("Intake Manifold Absol. Press.", "010B", pid010b           ,"kPa","psi"        ),
    Sensor("Engine RPM"                   , "010C", rpm               ,"rpm","rpm"        ),
    Sensor("Vehicle Speed"                , "010D", speed             ,"KmH","MPH"        ),
    Sensor("Timing Advance"               , "010E", timing_advance    ,"degrees","degrees"),
    Sensor("Intake Air Temp"              , "010F", temp              ,"deg. C","deg. F"  ),
    Sensor("Air Flow Rate - MAF"          , "0110", maf               ,"g/s","lb/s"       ),
    Sensor("Throttle Position"            , "0111", throttle_pos      ,"%","%"            ),
    Sensor("Secondary Air Status"         , "0112", cpass             ,"BitEnc.","BitEnc."),
    Sensor("Location of O2 sensors"       , "0113", cpass             ,"BitEnc.","BitEnc."),
    Sensor("O2 Sensor: 1 - 1"             , "0114", o2_sensor         ,"V#%","V#%"        ),
    Sensor("O2 Sensor: 1 - 2"             , "0115", o2_sensor         ,"V#%","V#%"        ),
    Sensor("O2 Sensor: 1 - 3"             , "0116", o2_sensor         ,"V#%","V#%"        ),
    Sensor("O2 Sensor: 1 - 4"             , "0117", o2_sensor         ,"V#%","V#%"        ),
    Sensor("O2 Sensor: 2 - 1"             , "0118", o2_sensor         ,"V#%","V#%"        ),
    Sensor("O2 Sensor: 2 - 2"             , "0119", o2_sensor         ,"V#%","V#%"        ),
    Sensor("O2 Sensor: 2 - 3"             , "011A", o2_sensor         ,"V#%","V#%"        ),
    Sensor("O2 Sensor: 2 - 4"             , "011B", o2_sensor         ,"V#%","V#%"        ),
    Sensor("OBD Designation"              , "011C", cpass             ,"BitEnc.","BitEnc."),
    Sensor("Location of O2 sensors"       , "011D", cpass             ,"BitEnc.","BitEnc."),
    Sensor("Aux input status"             , "011E", cpass             ,"BitEnc.","BitEnc."),
    Sensor("Time Since Engine Start"      , "011F", sec_to_min        ,"min.","min."      ),
    Sensor("Supported PIDs 2"             , "0120", hex_to_bitstring  ,"",""              ),
    Sensor("Distance traveled with MIL"   , "0121", speed             ,"Km","miles"       ),
    Sensor("Fuel Rail Press. vacuum   "   , "0122", FuelRailPress     ,"kPa","psi"        ),
    Sensor("Fuel Rail Pressure vacuum D." , "0123", FuelRailPressD    ,"kPa","psi"        ),
    Sensor("O2S1_WR_lambda - Volt"        , "0124", o2sv              ,"N/A - V","N/A - V"),
    Sensor("O2S2_WR_lambda - Volt"        , "0125", o2sv              ,"N/A - V","N/A - V"),
    Sensor("O2S3_WR_lambda - Volt"        , "0126", o2sv              ,"N/A - V","N/A - V"),
    Sensor("O2S4_WR_lambda - Volt"        , "0127", o2sv              ,"N/A - V","N/A - V"),
    Sensor("O2S5_WR_lambda - Volt"        , "0128", o2sv              ,"N/A - V","N/A - V"),
    Sensor("O2S6_WR_lambda - Volt"        , "0129", o2sv              ,"N/A - V","N/A - V"),
    Sensor("O2S7_WR_lambda - Volt"        , "012A", o2sv              ,"N/A - V","N/A - V"),
    Sensor("O2S8_WR_lambda - Volt"        , "012B", o2sv              ,"N/A - V","N/A - V"),
    Sensor("Commanded EGR"                , "012C", per100            ,"%","%"            ),
    Sensor("EGR Error"                    , "012D", per100_128        ,"%","%"            ),
    Sensor("Commanded evaporative purge"  , "012E", per100            ,"%","%"            ),
    Sensor("Fuel Level Input"             , "012F", per100            ,"%","%"            ),
    Sensor("n# of warm-ups s. codes clear", "0130", cpass             ,"",""              ),
    Sensor("Dist. traveled s. codes clear", "0131", speed             ,"KmH","MPH"        ),
    Sensor("Evap. System Vapor Pressure"  , "0132", pid0132           ,"Pa","psi"         ),
    Sensor("Barometric pressure"          , "0133", pid010b           ,"kPa","psi"        ),
    Sensor("O2S1_WR_lambda  current"      , "0134", o2sa              ,"N/A-mA","N/A-mA"  ),
    Sensor("O2S2_WR_lambda  current"      , "0135", o2sa              ,"N/A-mA","N/A-mA"  ),
    Sensor("O2S3_WR_lambda  current"      , "0136", o2sa              ,"N/A-mA","N/A-mA"  ),
    Sensor("O2S4_WR_lambda  current"      , "0137", o2sa              ,"N/A-mA","N/A-mA"  ),
    Sensor("O2S5_WR_lambda  current"      , "0138", o2sa              ,"N/A-mA","N/A-mA"  ),
    Sensor("O2S6_WR_lambda  current"      , "0139", o2sa              ,"N/A-mA","N/A-mA"  ),
    Sensor("O2S7_WR_lambda  current"      , "013A", o2sa              ,"N/A-mA","N/A-mA"  ),
    Sensor("O2S8_WR_lambda  current"      , "013B", o2sa              ,"N/A-mA","N/A-mA"  ),
    Sensor("Catalyst Temp. Bank1-Sensor1" , "013C", catT              ,"deg. C","deg. F"  ),
    Sensor("Catalyst Temp. Bank2-Sensor1" , "013D", catT              ,"deg. C","deg. F"  ),
    Sensor("Catalyst Temp. Bank1-Sensor2" , "013E", catT              ,"deg. C","deg. F"  ),
    Sensor("Catalyst Temp. Bank2-Senspr2" , "013F", catT              ,"deg. C","deg. F"  ),
    Sensor("Supported PIDs 3"             , "0140", hex_to_bitstring  ,"",""              ),
    Sensor("Monitor status drive cycle"   , "0141", cpass             ,"BitEnc.","BitEnc."),
    Sensor("Control module voltage"       , "0142", cmv               ,"V","V"            ),
    Sensor("Absolute load value"          , "0143", abl               ,"%","%"            ),
    Sensor("Command equivalence ratio"    , "0144", cer               ,"",""              ),
    Sensor("Relative throttle position"   , "0145", throttle_pos      ,"%","%"            ),
    Sensor("Ambient air temperature"      , "0146", temp              ,"deg. C","deg. F"  ),
    Sensor("Absolute throttle pos. B"     , "0147", throttle_pos      ,"%","%"            ),
    Sensor("Absolute throttle pos. C"     , "0148", throttle_pos      ,"%","%"            ),
    Sensor("Accelerator pedal pos. D"     , "0149", throttle_pos      ,"%","%"            ),
    Sensor("Accelerator pedal pos. E"     , "014A", throttle_pos      ,"%","%"            ),
    Sensor("Accelerator pedal position F" , "014B", throttle_pos      ,"%","%"            ),
    Sensor("Commanded throttle actuator"  , "014C", throttle_pos      ,"%","%"            ),
    Sensor("Time run with MIL on"         , "014D", cpass             ,"min.","min."      ),
    Sensor("Time since codes cleared"     , "014E", cpass             ,"min.","min."      ),
    Sensor("Max  val for various ratio...", "014F", max               ,"V,mA,kPa","V,mA,kPa"),
    Sensor("Max val air flow r. from MAF" , "0150", pid150            ,"g/s","g/s"        ),
    Sensor("Fuel Type(Bit Encoded)"       , "0151", pid151            ,"BitEnc.","BitEnc."),
    Sensor("Ethanol fuel %"               , "0152", throttle_pos      ,"%","%"            ),
    Sensor("Absol. EvapSystemVapourPress.", "0153", cpass             ,"1/200*bit","1/200*bit"),
    Sensor("Evap system vapor pressure"   , "0154", pid154            ,"Pa","psi"         ),
    Sensor("Short term 2 oxygen se. b1&b3", "0155", oxsens2           ,"%","%"            ),
    Sensor("Long term 2 oxygen sen. b1&b3", "0156", oxsens2           ,"%","%"            ),
    Sensor("Short term 2 oxygen se. b2&b4", "0157", oxsens2           ,"%","%"            ),
    Sensor("Long term 2 oxygen sen. b2&b4", "0158", oxsens2           ,"%","%"            ),
    Sensor("Fuel rail pressure(absolute)" , "0159", FuelRailPressD    ,"kPa","psi"        ),
    Sensor("Relative acceler. pedal pos." , "015A", throttle_pos      ,"%","%"            ),
    Sensor("Hybrid battery remaining life", "015B", throttle_pos      ,"%","%"            ),
    Sensor("Engine oil temperature"       , "015C", temp              ,"deg. C","deg. F"  ),
    Sensor("Fuel injection timing"        , "015D", pid15d            ,"degrees","degrees"),
    Sensor("Engine fuel rate"             , "015E", pid15e            ,"L/h","gal/h"      ),
    Sensor("Emission req. to Vehicle Des.", "015F", cpass             ,"BitEnc.","BitEnc."),

    ]
     
    
#___________________________________________________________

def test():
    for i in SENSORS:
        print i.name, i.value("F")

if __name__ == "__main__":
    test()
