import serial
import platform

import py_obd.obd_io as obd_io
import py_obd.obd_sensors as obd_sensors
from py_obd.obd_utils import scanSerial

class OBD_Poller():
    def __init__(self, log_items):
        self.port = None
        self.sensorlist = []

        for item in log_items:
            self.add_log_item(item)

    def connect(self):
        portnames = scanSerial()
        print portnames
        for port in portnames:
            self.port = obd_io.OBDPort(port, None, 2, 2)
            if(self.port.State == 0):
                self.port.close()
                self.port = None
            else:
                break

        if(self.port):
            print "MIP: Connected to "+self.port.port.name
            
    def is_connected(self):
        return self.port
        
    def add_log_item(self, item):
        for index, e in enumerate(obd_sensors.SENSORS):
            if(item == e.name):
                self.sensorlist.append(index)
                print "MIP: Logging item: " + e.name
                break
            
            
    def get_data(self, index):
        if(self.port is None):
            return 0
        else:
            (name, value, unit, unit2) = self.port.sensor(index)
            return value[0]


